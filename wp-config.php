<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'klariti_db');

/** MySQL database username */
define('DB_USER', 'klariti_db');

/** MySQL database password */
define('DB_PASSWORD', 'e#66f8XKvo5k');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5-`D>al5PCJtL0ET/-M<5TQJy)`68K7uL(_!a%4sXa#.6!+9>SUqDFyZYM9i221J');
define('SECURE_AUTH_KEY',  '{9]v[DJO&)8lpQ_{cTZdtwMU|?W1?GoS6XB@Dt?N6`Sf*CZ96o1;u1!#FNT2yyZw');
define('LOGGED_IN_KEY',    'T0mF^;|[BBX7?4Mlbk*XIe=M9=4E[AH%eZUVT[@$YF6:+b-7$pTr.mdLb?zC#[1s');
define('NONCE_KEY',        'EuIkusd&bY(ZLlexkDMN$/p y]u+*K9l/%JV,`29c>W<>+|Lr I`/N;%g BfYj$;');
define('AUTH_SALT',        'CSx`Nv2L!,#K/)y3Df1LksjFI4[_Wh!-b<LGILneQnO93mot1R0cr<MjN%.9L Te');
define('SECURE_AUTH_SALT', 'Akd!Ga&P9C4|K&#o*N6#qbm?kQTp=_Njm!j0`^;W[6T/kMA=fe%+PR$haLJ{Y:kx');
define('LOGGED_IN_SALT',   'yHqtdrj<Pidc&f2@bZ%NRhVtGAS]n,/ .2HUIZcO;t}3[G)1<K~CY=@g0E{MPYm:');
define('NONCE_SALT',       'OzsjJ0_dF=%.4@!Ggbkp,#K}a-<v:H[z%fkgebCoqSf||fJ[+CbZoQ47hqQ!s-LK');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
