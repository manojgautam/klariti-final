<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
</div><!-- .site-content -->
		<footer id="footer" class="site-footer" role="contentinfo">
			<div class="container">
			<div class="row text-left">
			<div class="col-sm-12"><?php dynamic_sidebar( 'sidebar-2' ); ?></div>
             
			</div><!-- .row -->
			<div class="site-info">
			<?php dynamic_sidebar( 'sidebar-6' ); ?>
			</div><!-- .site-info -->
			</div><!-- .container -->
<div class="callus">
<a href="#modal"><i class="fa fa-phone"></i></a>
</div>
<div class="enqueryform">
<a href="#enquiry">Enquiry Form</a>
</div>
</footer><!-- .site-footer -->
<!----------Call-Us-popup------------->
<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
<form id="callus" role="form" action="" method="post">
<div class="callus-popup">
<h1>Please fill the information. We will contact you with in 10 minute.
<div class="popup-field">
<label class="labl-name">Your Name</label><p></p>
<div class="popup-field-name">
<input type="text" id="name" name="your-name" value=""  class="form-control" required>
<span class="rq" style='display:none'>*Required Field</span>  
</div>
</div>
<div class="popup-field">
<label class="labl-name">Phone Number</label><p></p>
<div class="popup-field-name">
<input type="text" id="phone" name="phno" value=""  class="form-control" required>
<span class="rq1" style='display:none'>*Required Field</span>  
</div>
</div>
</div>
<br>
<div class="sub-btn">
<button type="button" class="remodal-confirm">Submit</button>
</div>
</form>
<span id=show_res> </span>
</div>
<!--call-us----->

<!---------- Enquiry Forms-popup------------->
<div class="remodal" data-remodal-id="enquiry" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
<div class="enquery-form"><?php echo do_shortcode('[contact-form-7 id="1147" title="Enquiry Form"]');?></div>
<span id=show_res> </span>
</div>
<!-- Enquiry Forms----->

	</div><!-- .site-inner -->
</div><!-- .site -->
<?php wp_footer(); ?>
<script type='text/javascript' src='<?php echo bloginfo('template_directory')?>/js/remodal.min.js'></script>
<script type="text/javascript" src="<?php echo bloginfo('template_directory')?>/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?php echo bloginfo('template_directory')?>/js/jasny-bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo bloginfo('template_directory')?>/js/unslider.js"></script>
<script src="<?php echo bloginfo('template_directory')?>/js/jquery.dataTables.min.js"></script>
<script src="<?php echo bloginfo('template_directory')?>/js/dataTables.bootstrap.min.js"></script>
<script>
jQuery(window).scroll(function() {
if (jQuery(this).scrollTop() > 1){  
   jQuery('header').addClass("sticky");
  }
  else{
    jQuery('header').removeClass("sticky");
  }
});
</script>
<script>
jQuery('.my-slider').unslider({
animation: 'vertical',
autoplay: true,
infinite: true
});
</script>
<script>
    jQuery('a[href="#search"]').on('click', function(event) {
        event.preventDefault();
        jQuery('#search').addClass('open');
        jQuery('#search > form > input[type="search"]').focus();
    });
    
    jQuery('#search, #search button.close').on('click keyup', function(event) {
        if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
            jQuery(this).removeClass('open');
        }
    });
    
</script>
<script>
jQuery(function(){
jQuery(".remodal-confirm").click(function(e){
var name = jQuery("#name").val();
var phone = jQuery("#phone").val();
if(name==""){
jQuery(".rq").show();
}else if(phone==""){
jQuery(".rq1").show();
}else{
jQuery.ajax({
method: "POST",
url: "/ajax.php",
data: { 'name' :name ,'phone':phone}
})
.done(function( msg ) {
//alert(msg );
jQuery("#callus").css("display", "none");
jQuery("#show_res").html(msg);
});
}
});
});
</script>
<script>
jQuery('.remodal-close').click(function () {
jQuery("#show_res").hide();
jQuery("#callus").show();
});
</script>
<script>
jQuery(".form-control").click(function () {
jQuery('.rq').hide();
jQuery('.rq1').hide();
});
</script>
<script>
jQuery(".remodal-close").click(function () {
jQuery('#callus')[0].reset();
jQuery("#name").text("");
jQuery("#phone").text("");
});
</script>
<script>
		/* Top bar Start */
jQuery('.top-bar .close').on('click', function() {
    var el = jQuery('.top-bar');
    var child = jQuery('.top-bar .container');
    el.toggleClass('open');  

    if( el.hasClass('open') ) {
      child.height(child .attr('data-height'));
    } else {
      child.attr('style', 'height: 0;');
    }
  });
  topBarSize();
  jQuery(window).resize(function() {
    topBarSize();
});
  
function topBarSize() {
    var el = jQuery('.top-bar .container');
    el.css( {
      'display' : 'none',
      'height'  : 'auto' 
    }).attr('data-height', el.height()).attr('style', 'height: 0;').parent().removeClass('open');
  }
/* // Top bar End   */
</script>
<script>
		jQuery('#incfont').click(function(){	   
        curSize= parseInt(jQuery('body, .service-tab h3').css('font-size')) + 2;
		if(curSize<=20)
        jQuery('body, .service-tab h3').css('font-size', curSize);
        });  
		jQuery('#decfont').click(function(){	   
        curSize= parseInt(jQuery('body, .service-tab h3').css('font-size')) - 2;
		if(curSize>=12)
       jQuery('body, .service-tab h3').css('font-size', curSize);
        }); 

</script>
<script>
// Tooltip
jQuery('[data-toggle="tooltip"]').tooltip(); 
// Tooltip
</script>
<script>
 jQuery(function () {
    jQuery("#example").DataTable();
    jQuery('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
<script>
jQuery(document).ready(function() {
var str = window.location.href;
var queryString = str.split('?');

if(typeof queryString[1]=== "undefined"){
    
   }
else{

var selectvalue=queryString[1];
 var res = selectvalue.replace("%20", " ");

  jQuery("#menu-drop > option").each(function() {
    if (this.value == res) {
      this.selected = 'selected';
    }
  });
}
});
</script>
<script type="text/javascript">
jQuery(document).ready(function () {
    //Disable cut copy paste
    jQuery('body').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
      //Disable mouse right click
    jQuery("body").on("contextmenu",function(e){
        return false;
    });
   
});

</script>
<script>
jQuery(document).ready(function() {

    var $answers = jQuery('div.submenublock > ul');
    jQuery("div.submenublock > h3").click(function () {
        var $ans = jQuery(this).next("div.submenublock > ul").stop(true).slideToggle('fast');
        $answers.not($ans).filter(':visible').stop(true).slideUp();
});

});
</script>


<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/59f6e7a14854b82732ff8c81/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

</body>
</html>