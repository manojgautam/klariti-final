<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo bloginfo('template_directory')?>/css/jasny-bootstrap.min.css" type="text/css" media="all" >
	<link href="<?php echo bloginfo('template_directory')?>/css/navmenu-reveal.css" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo bloginfo('template_directory')?>/css/bootstrap.min.css" type="text/css" media="all" >
	<link rel="stylesheet" href="<?php echo bloginfo('template_directory')?>/css/unslider.css" type="text/css" media="all" >
	<link rel="stylesheet" href="<?php echo bloginfo('template_directory')?>/css/navmenu-reveal.css" type="text/css" media="all" >
<link rel='stylesheet' id='transfers-style-main-css'  href='<?php echo bloginfo('template_directory')?>/css/remodal-default-theme.css' type='text/css' media='all' />
<link rel='stylesheet' id='transfers-style-main-css'  href='<?php echo bloginfo('template_directory')?>/css/remodal.css' type='text/css' media='all' />
	<?php wp_head(); ?>
	<style>
	.slider {
    background: url(/wp-content/uploads/2017/02/slider_img.jpg);
    height: 570px;
    background-size: cover;
    background-repeat: no-repeat;
}
	  </style>
</head>

<body <?php body_class(); ?>>

<div class="navmenu navmenu-default navmenu-fixed-left">
     <a href="http://klariti.in/" class="navmenu-brand custom-logo-link" rel="home" itemprop="url"><img width="124" height="40" src="/wp-content/themes/klariti/images/logo_final_text.svg" class="custom-logo" alt="klariti- logo" itemprop="logo"></a>
     
						<?php if ( has_nav_menu( 'canvas' ) ) : ?>
							<div id="site-navigation" class="navmenu-default " role="navigation" aria-label="<?php esc_attr_e( 'Canvas Menu', 'twentysixteen' ); ?>">
								<?php
									wp_nav_menu( array(
										'theme_location' => 'canvas',
										'menu_class'     => 'nav navmenu-nav',
									 ) );
								?>
						
						
						
							</div><!-- .main-navigation -->
						<?php endif; ?>

    </div>
	<div id="search">
    <button type="button" class="close">×</button>
<form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'twentysixteen' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	<button type="submit" class="btn btn-primary">Search</button>
</form>
</div>
<div class="canvas">
<div id="page" class="site">
	<div class="site-inner">

		<header id="masthead" class="site-header" role="banner">
		<div class="top-bar top-bar-white">
        <div class="container-fluid">
            <div class="row">
        <!-- Left Section -->
            
            <!-- Left Section -->
            <!-- Right Section -->
            <div class="top-bar-right">
              
				<div style="padding:0" class="col-sm-12">
                <ul class="right-top font-resizer visible-lg visible-md">
                    <li><a href="mailto:contact@klariti.in"><i class="fa fa-envelope-open" aria-hidden="true"></i>contact@klariti.in</a></li>
                    <li><a href="tel:+91-9646977822"><i class="fa fa-phone"></i> +91-9646977822</a></li>
                    <li><a href="tel:+91-7009865743"><i class="fa fa-phone"></i> +91-7009865743</a></li>
                    <li class="aside marbot0 visible-block-md-xs">Font : </li>
				    <li><a href="javascript:void(0)" id="decfont" class="button buttonfont">A-</a></li>
                    <li> <a href="javascript:void(0)" id="incfont" class="button buttonfont">A+</a></li>

					 <!-- <li><a href="#" class="decrease" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Decrease Font"><i class="fa">A-</i></a></li>
                  <li><a href="#" class="resetMe" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Default Font"><i class="fa">A</i></a></li>
                    <li><a href="#" class="increase" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Increase Font"><i class="fa">A+</i></a></li>-->
                </ul>
				</div>
             </div>
             <!-- Right Section -->
            </div>
        </div>
        <span class="close fa fa-chevron-down"></span>    
		</div>
		<nav class="navbar">
		<div class="container-fluid">
			<div class="site-header-main">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="navbar-brand">
					<?php //twentysixteen_the_custom_logo(); ?>
<a href="http://klariti.in/" class="custom-logo-link" rel="home" itemprop="url"><img width="124" height="40" src="/wp-content/themes/klariti/images/logo_final_text.svg" class="custom-logo" alt="klariti- logo" itemprop="logo"></a>
				</div>
			</div>
				
			

					<div id="bs-example-navbar-collapse-1" class="site-header-menu collapse navbar-collapse">
						<?php if ( has_nav_menu( 'primary' ) ) : ?>
							<div id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php //esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
								<?php
									//wp_nav_menu( array(
										//'theme_location' => 'primary',
										//'menu_class'     => 'nav navbar-nav main-nav',
									// ) );
								?>
								<ul class="nav navbar-nav navbar-right">
<li class="gtransl"><?php echo do_shortcode('[google-translator]'); ?></li>
					<li><a href="#" data-toggle="modal" data-target="#modal1" class="auth">Sign Up</a></li>
					<li><a href="#" data-toggle="modal" data-target="#modal2" class="auth">Login</a></li>
					<li><a href="#search" class="search-tog"><i class="fa fa-search" aria-hidden="true"></i></a></li>
					
					<li class="hidden-sm hidden-xs">
					<a href="javascript:void(0)" class="ham-tog" data-toggle="offcanvas" data-recalc="false" data-target=".navmenu" data-canvas=".canvas"><i class="fa fa-bars" aria-hidden="true"></i></a></li>
					
						</ul>
						
						
							</div><!-- .main-navigation -->
						<?php endif; ?>

					</div><!-- .site-header-menu -->
			
					
			</div><!-- .site-header-main -->

			</div>
			</nav>
		</header><!-- .site-header -->
		
<!-- .site-slider -->
 <!--<div class="slider">
 <?php //$klariti = get_option('theme_mods_klariti-child');?>  
	<img src="<?php //echo $klariti['Background']; ?>">
	<div class="slider-text">
	<div class="slider-inner">
		<div class="slider-desc">
		<h1><?php //echo $klariti['Bigtitle']; ?></h1>
		<p><?php //echo $klariti['Description']; ?></p>
		</div>
		<div class="get-started">
		<a href="<?php //echo $klariti['Contactlink']; ?>"><?php //echo $klariti['Contactus']; ?></a>
		</div>
		</div>
	</div> 
</div>-->
<!-- .site-slider -->

		<div id="content" class="site-content">
