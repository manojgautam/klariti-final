<?php
/* Template Name: Loan Landing */

get_header();
?>
<div class="inner-header">
<div class="col-sm-12 col-xs-12 landing-loan-img"><?php twentysixteen_post_thumbnail('full'); ?></div></div>
<?php
the_content();

get_footer();
