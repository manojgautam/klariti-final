<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header1">
				<?php
					//the_archive_title( '<h1 class="page-title">', '</h1>' );
					//the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			
			<div class="inner-header"><div class="col-sm-4 col-xs-12 head-cont"><h2 class="entry-title"><?php the_archive_title(); ?></h2><div class="bread"><a href="http://klariti.in" rel="nofollow">Home</a>&nbsp;&nbsp;»&nbsp;&nbsp;Blog</div></div>
<div class="col-sm-8 col-xs-12 head-img">
	<div class="post-thumbnail">
		<img src="http://klariti.in/wp-content/uploads/2017/03/slider_img1-1348x270.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="slider_img1" srcset="http://klariti.in/wp-content/uploads/2017/03/slider_img1-1348x270.jpg 1348w, http://klariti.in/wp-content/uploads/2017/03/slider_img1-300x60.jpg 300w, http://klariti.in/wp-content/uploads/2017/03/slider_img1-768x154.jpg 768w, http://klariti.in/wp-content/uploads/2017/03/slider_img1-1024x205.jpg 1024w, http://klariti.in/wp-content/uploads/2017/03/slider_img1.jpg 1349w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px">	</div><!-- .post-thumbnail -->
</header><!-- .page-header -->
<div class="container blog_page">
			<?php
			// Start the Loop.
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );

			// End the loop.
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'twentysixteen' ),
				'next_text'          => __( 'Next page', 'twentysixteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
			) );

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
</div>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
