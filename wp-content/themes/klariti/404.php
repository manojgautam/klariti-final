<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area container">
		<main id="main" class="site-main" role="main">

			<!-- <section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php //_e( 'Oops! That page can&rsquo;t be found.', 'twentysixteen' ); ?></h1>
				</header>

				<div class="page-content">
					<p><?php //_e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentysixteen' ); ?></p>

					<?php //get_search_form(); ?>
				</div>
			</section> -->
			
			<div class="hero-unit center">
          <h1>Page Not Found <small><font face="Tahoma" color="red">Error 404</font></small></h1>
          <br />
          <p>The page you requested could not be found, either contact your webmaster or try again. Use your browsers <b>Back</b> button to navigate to the page you have prevously come from</p>
          <p><b>Or you could just press this neat little button:</b></p>
          <a href="http://klariti.in/" class="btn btn-large btn-info"><i class="fa fa-home" aria-hidden="true"></i>&nbsp; Take Me Home</a>
        </div>

		</main><!-- .site-main -->

		<?php //get_sidebar( 'content-bottom' ); ?>

	</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
