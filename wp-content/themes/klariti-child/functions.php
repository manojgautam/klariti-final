<?php
//
// Recommended way to include parent theme styles.
//  (Please see http://codex.wordpress.org/Child_Themes#How_to_Create_a_Child_Theme)
//  
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array('parent-style')
    );
}
//
// Your code goes below
//




//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Tabs Part
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function Tabsing_customize_register( $wp_customize ) {
  $wp_customize->add_setting('Tabs', array());
		 
  //******************************************************
  // General Organic Detail
  //******************************************************
   
   $wp_customize->add_control( new WP_Customize_search_Section_Control( 
    $wp_customize, 'Email_Secs', 
    array(
      'label' => __( 'Organic Section', 'tech' ),
      'section' => 'Tabs',
      'settings' => 'Tabs',
    ) 
  ));
  
    $wp_customize->add_setting('Backgroundimage', array());
  $wp_customize->add_control(new WP_Customize_Image_Control(
    $wp_customize,'Backgroundimage',
             array(
                 'label'       => __( 'Tab1 image', 'tech' ),
                 'section'     => 'Tabs',
                 'settings'    => 'Backgroundimage',
                 'context'     => 'Backgroundimage',
                 'description' => '1000 x 400px recommended', 
             )
         ));

  $wp_customize->add_setting('OrganicTitle', array());
  $wp_customize->add_control('OrganicTitle', array(
    'label'       => __('Enter Title First ', 'tech'),
    'section'     => 'Tabs',
    'settings'    => 'OrganicTitle',
    'type'        => 'text',
  ));

  $wp_customize->add_setting('OrganicSubTitle', array());
  $wp_customize->add_control('OrganicSubTitle', array(
    'label'       => __('Enter Second Title', 'tech'),
    'section'     => 'Tabs',
    'settings'    => 'OrganicSubTitle',
    'type'        => 'text',
  )); 
   
  //******************************************************
  // Free No Detail
  //******************************************************
   
   $wp_customize->add_control( new WP_Customize_search_Section_Control( 
    $wp_customize, 'Free_Secs', 
    array(
      'label' => __( 'Free Section', 'tech' ),
      'section' => 'Tabs',
      'settings' => 'Tabs',
    ) 
  ));

      $wp_customize->add_setting('Backgroundimage2', array());
  $wp_customize->add_control(new WP_Customize_Image_Control(
    $wp_customize,'Backgroundimage2',
             array(
                 'label'       => __( 'Tab2 image', 'tech' ),
                 'section'     => 'Tabs',
                 'settings'    => 'Backgroundimage2',
                 'context'     => 'Backgroundimage2',
                 'description' => '1000 x 400px recommended', 
             )
         ));
  $wp_customize->add_setting('CallNow', array());
  $wp_customize->add_control('CallNow', array(
    'label'       => __('Title', 'tech'),
    'section'     => 'Tabs',
    'settings'    => 'CallNow',
    'type'        => 'text',
  ));

  $wp_customize->add_setting('FreeNo', array());
  $wp_customize->add_control('FreeNo', array(
    'label'       => __('Sub Title', 'tech'),
    'section'     => 'Tabs',
    'settings'    => 'FreeNo',
    'type'        => 'text',
  ));


  //******************************************************
  // Opening Premium Detail
  //******************************************************
   
   $wp_customize->add_control( new WP_Customize_search_Section_Control( 
    $wp_customize, 'Hour_Section', 
    array(
      'label' => __( 'Premium Section', 'tech' ),
      'section' => 'Tabs',
      'settings' => 'Tabs',
    ) 
  ));

      $wp_customize->add_setting('Backgroundimage3', array());
  $wp_customize->add_control(new WP_Customize_Image_Control(
    $wp_customize,'Backgroundimage3',
             array(
                 'label'       => __( 'Tab3 image', 'tech' ),
                 'section'     => 'Tabs',
                 'settings'    => 'Backgroundimage3',
                 'context'     => 'Backgroundimage3',
                 'description' => '1000 x 400px recommended', 
             )
         ));
  $wp_customize->add_setting('PremiumUs', array());
  $wp_customize->add_control('PremiumUs', array(
    'label'       => __('Title', 'tech'),
    'section'     => 'Tabs',
    'settings'    => 'PremiumUs',
    'type'        => 'text',
  ));

  $wp_customize->add_setting('City', array());
  $wp_customize->add_control('City', array(
    'label'       => __('Sub Title', 'tech'),
    'section'     => 'Tabs',
    'settings'    => 'City',
    'type'        => 'text',
  )); 



   
     $wp_customize->add_section('Tabs' , array(
      'title' => __('Tabs','tech'),
  ));
}
add_action( 'customize_register', 'Tabsing_customize_register' );

if( class_exists( 'WP_Customize_Control' ) ):

class WP_Customize_search_Section_Control extends WP_Customize_Control { 
    public function render_content() {
    ?>
      <hr>
      <h1><?php echo esc_html( $this->label ); ?></h1>
    <?php
    }
  }
endif;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Custom functions End
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Slider Part
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function Tabsing_customize_slider( $wp_customize ) {
  $wp_customize->add_setting('Slider', array());
     
	 //******************************************************
  // Address Detail
  //******************************************************
   
   $wp_customize->add_control( new WP_Customize_slider_Section_Control( 
    $wp_customize, 'Address_Section', 
    array(
      'label' => __( 'Address Section', 'tech' ),
      'section' => 'Slider',
      'settings' => 'Slider',
    ) 
  ));
  
        $wp_customize->add_setting('Background', array());
  $wp_customize->add_control(new WP_Customize_Image_Control(
    $wp_customize,'Background',
             array(
                 'label'       => __( 'Slider image', 'tech' ),
                 'section'     => 'Slider',
                 'settings'    => 'Background',
                 'context'     => 'Background',
                 'description' => '1000 x 400px recommended', 
             )
         ));
  
  $wp_customize->add_setting('Bigtitle', array());
  $wp_customize->add_control('Bigtitle', array(
    'label'       => __('Big Title', 'tech'),
    'section'     => 'Slider',
    'settings'    => 'Bigtitle',
    'type'        => 'text',
  ));

  $wp_customize->add_setting('Description', array());
  $wp_customize->add_control('Description', array(
    'label'       => __('Description', 'tech'),
    'section'     => 'Slider',
    'settings'    => 'Description',
    'type'        => 'textarea',
  ));

  $wp_customize->add_setting('Contactus', array());
  $wp_customize->add_control('Contactus', array(
    'label'       => __('Button Text', 'tech'),
    'section'     => 'Slider',
    'settings'    => 'Contactus',
    'type'        => 'text',
  ));
  
    $wp_customize->add_setting('Contactlink', array());
  $wp_customize->add_control('Contactlink', array(
    'label'       => __('Link For Button', 'tech'),
    'section'     => 'Slider',
    'settings'    => 'Contactlink',
    'type'        => 'text',
  ));
 

	 
 

   
     $wp_customize->add_section('Slider' , array(
      'title' => __('Slider Text','tech'),
  ));
}
add_action( 'customize_register', 'Tabsing_customize_slider' );

if( class_exists( 'WP_Customize_Control' ) ):


class WP_Customize_slider_Section_Control extends WP_Customize_Control { 
    public function render_content() {
    ?>
      <hr>
      <h1><?php echo esc_html( $this->label ); ?></h1>
    <?php
    }
  }
endif;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Slider Custom functions End
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function Copying_customize_register( $wp_customize ) {
  $wp_customize->add_setting('Copy', array());
		 
  //******************************************************
  // General Email Detail
  //******************************************************
   
   $wp_customize->add_control( new WP_Customize_copy_Section_Control( 
    $wp_customize, 'Copy_write', 
    array(
      'label' => __( 'Copy Write Section', 'tech' ),
      'section' => 'Copy',
      'settings' => 'Copy',
    ) 
  ));
     $wp_customize->add_setting('FooterImage', array());
  $wp_customize->add_control(new WP_Customize_Image_Control(
    $wp_customize,'FooterImage',
             array(
                 'label'       => __( 'Footer Logo', 'tech' ),
                 'section'     => 'Copy',
                 'settings'    => 'FooterImage',
                 'context'     => 'FooterImage',
             )
         ));

  $wp_customize->add_setting('CopyWrite', array());
  $wp_customize->add_control('CopyWrite', array(
    'label'       => __('Enter Copy write ', 'tech'),
    'section'     => 'Copy',
    'settings'    => 'CopyWrite',
    'type'        => 'textarea',
  ));


  
     $wp_customize->add_section('Copy' , array(
      'title' => __('Copy','tech'),
  ));
}
add_action( 'customize_register', 'Copying_customize_register' );

if( class_exists( 'WP_Customize_Control' ) ):

class WP_Customize_copy_Section_Control extends WP_Customize_Control { 
    public function render_content() {
    ?>
      <hr>
      <h1><?php echo esc_html( $this->label ); ?></h1>
    <?php
    }
  }
  endif;


add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3 );

function remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}

function get_breadcrumb() {
    echo '<a href="'.home_url().'" rel="nofollow">Home</a>';
    if (is_category() || is_single()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        the_category(' &bull; ');
            if (is_single()) {
                echo " &nbsp;&nbsp;&#187;&nbsp;&nbsp; ";
                the_title();
            }
    } elseif (is_page()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        echo the_title();
    } elseif (is_search()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;Search Results for... ";
        echo '"<em>';
        echo the_search_query();
        echo '</em>"';
    }
}

// stop plugin updates

remove_action('load-update-core.php','wp_update_plugins');
add_filter('pre_site_transient_update_plugins','__return_null');