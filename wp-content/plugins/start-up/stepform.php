<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
<?php
   
global $wpdb;
$startup_table = $wpdb->prefix.'start_up';

          $sql = "SELECT * FROM $startup_table ";                  

          $result=$wpdb->get_results($sql);

 ?>
<div class="table-responsive">      
 <table id="example" class="table table-striped table-bordered" cellspacing="0" >
		  <thead>
					<tr>
						<th >State</th>
						<th >TYPE OF COMPANY</th>
						<th >Company Name</th>
						<th >Directors</th>
						<th >Shareholders</th>
						<th >Name</th>
						<th >Telephone</th>
						<th >Email</th>
						<th >Address</th>
						<th >Advance Paid</th>
						<th >TotalAmount</th>
					</tr>
		  </thead>
		  <tbody>
<?php foreach($result as $val){ ?>
					<tr>
						
						<td ><?php echo $val->state;?></td>
						<td ><?php echo $val->company_type;?></td>
						<td ><?php echo $val->company_name;?></td>
						<td ><?php echo $val->director;?></td>
						<td ><?php echo $val->shareholder;?></td>
						<td ><?php echo $val->personal_name;?></td>
						<td ><?php echo $val->telephone;?></td>
						<td ><?php echo $val->email;?></td>
						<td ><?php echo $val->address;?></td>
						<td ><?php echo $val->paidamount;?></td>
						<td ><?php echo $val->totalamount;?></td>
					</tr>
<?php } ?>
					
                   </tbody>
				</table>
</div>
<script src="http://klariti.in/wp-content/themes/klariti/js/jquery.dataTables.min.js"></script>
<script src="http://klariti.in/wp-content/themes/klariti/js/dataTables.bootstrap.min.js"></script>
<script>
 jQuery(function () {
      jQuery('#example').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });

  });
</script>