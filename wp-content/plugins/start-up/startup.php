<?php
   /**
    * Plugin Name: Start Up
    * Plugin URI: 
    * Description: This plugin is used for start up form. Shortcode:[startup] 
    * Version: 1.0.0
    * Author: Chandni Handa
    * Author URI: 
    * License: GPLv2
    */
   //define('WP_DEBUG',true); 
   $siteurl = get_option('siteurl');
   define('PLD_FOLDER', dirname(plugin_basename(__FILE__)));
   define('PLD_URL', $siteurl.'/wp-content/plugins/' . PLD_FOLDER);
   define('PLD_FILE_PATH', dirname(__FILE__));
   define('PLD_DIR_NAME', basename(PLD_FILE_PATH));
   
   register_activation_hook(__FILE__,'simple_optimization_cron_on'); // run simple_optimization_cron_on at plugin activation
   register_deactivation_hook(__FILE__,'simple_optimization_cron_off'); // run simple_optimization_cron_off at plugin deactivation
   
   //add menu
   add_action( 'admin_menu', 'start_top_menu' );
    function start_top_menu(){
   add_menu_page('Start Up', 'Start Up', 'manage_options', __FILE__, 'registered_user');

   }
   function registered_user(){
    include_once( plugin_dir_path( __FILE__ ) . 'stepform.php' );
   }
   
   function form_startup(){
   ?>
<link rel="stylesheet" href="<?php echo PLD_URL;?>/wizard.css">
<script type="text/javascript" src="<?php echo PLD_URL;?>/wizard.js"></script>

<div class="image-container set-full-height">
   <!--   Big container   -->
   <div class="container">
      <div class="row">
         <div class="col-sm-12">

          <!--php code start here insert query-->
<?php

if(isset($_POST['submit'])){
	print_r($_POST); 
	if($_POST['state'] == '' || $_POST['companytype'] == '' || $_POST['proposal-name'] == '' || $_POST['telephone-no'] == '' || $_POST['proposal-email'] == ''){
		 wp_redirect("http://klariti.in/start-up/");
	} else {

  $state = $_POST['state'];
  $companytype = $_POST['companytype'];
   $companyname = $_POST['company-name'];
   $companyname =json_encode($companyname);
  $numberofdirectors = $_POST['number-of-directors'];
  $numberofshareholders = $_POST['number-of-shareholders'];
  $mainobject = $_POST['main-object'];
  $proposalname = $_POST['proposal-name'];
  $telephoneno = $_POST['telephone-no'];
  $proposalemail = $_POST['proposal-email'];
  $address = $_POST['address'];
  $paidamount=$_POST['paidamount'];
  $totalamount=$_POST['totalamount'];
  $payementtype =$_POST['payementtype'];
  $transactionid= substr(md5(microtime()),rand(0,26),8);;
  
   
global $wpdb;
$startup_table = $wpdb->prefix.'start_up';

          $sql = "INSERT INTO $startup_table ( `state`, `company_type`, `company_name`, `director`, `shareholder`, `main_object`, `personal_name`, `telephone`, `email`, `address`, `paidamount`, `totalamount`, `payement_type`, `transaction_id`) VALUES ( '$state', '$companytype', '$companyname', '$numberofdirectors', '$numberofshareholders', '$mainobject', '$proposalname', '$telephoneno', '$proposalemail', '$address','$paidamount', ' $totalamount', '$payementtype', '$transactionid')";                  

          $result=$wpdb->query($sql);

if($result)
{
echo "<h4>Thanks for the Registeration. We will contact you.</h4>";
$admin_email= get_option( 'admin_email' );
$to      = 'chandni.techindustan@gmail.com';
$subject = 'Registered Company';
$companyarray= json_decode($companyname);
$companynamedata='<tr> <td rowspan="'.(count($companyarray)+1).'"><strong>Proposed name of company</strong></td></tr>';
foreach($companyarray as $value){
$companynamedata.= '
    <tr>
      <td>'.$value.'</td>
    </tr>'; 
}
$message = 'Hello Klariti,<br/> <strong>'.$proposalname.'</strong> want to register a Company.<br/>  ';
$message .= '<table width="100%" border="1" cellpadding="5" style="border-collapse:collapse" bgcolor="#FFFFFF">
   <tbody>
      <tr>
         <td><strong>STATE OF INCORPORATION</strong></td>
         <td>'.$state.'</td>
      </tr>
      <tr>
         <td><strong>TYPE OF COMPANY</strong></td>
         <td>'.$companytype.'</td>
      </tr>
      '.$companynamedata.'
      <tr>
         <td><strong>Number of Directors</strong></td>
         <td>'.$numberofdirectors.'</td>
      </tr>
      <tr>
         <td><strong>Number of Shareholders</strong></td>
         <td>'.$numberofshareholders.'</td>
      </tr>
      <tr>
         <td><strong>Main objects of company</strong></td>
         <td>'.$mainobject.'</td>
      </tr>
      <tr>
         <td><strong>Personal Name</strong></td>
         <td>'.$proposalname.'</td>
      </tr>
      <tr>
         <td><strong>Telephone No</strong></td>
         <td>'.$telephoneno.'</td>
      </tr>
      <tr>
         <td><strong>Email </strong></td>
         <td>'.$proposalemail.'</td>
      </tr>
       <tr>
         <td><strong>Address</strong></td>
         <td>'.$address.'</td>
      </tr>
       <tr>
         <td><strong>Advance Paid</strong></td>
         <td>'.$paidamount.'</td>
      </tr>
       <tr>
         <td><strong>Total Amount</strong></td>
         <td>'.$totalamount.'</td>
      </tr>
   </tbody>
</table>';
$headers = "From: Klariti <contact@klariti.in>". "\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
wp_mail($to, $subject, $message,$headers);
}
           
            wp_redirect("http://klariti.in/registered?orderid=".$transactionid);

die;
}
}
  

 ?>

<!--Php code end here-->
            <!-- Wizard container -->
            <div class="wizard-container">
               <div class="card wizard-card" data-color="red" id="wizard">
                  <form action="" method="post">
                     <div class="wizard-navigation">
                        <ul>
                           <li><a href="#details" data-toggle="tab">Type Your Company</a></li>
                           <li><a href="#captain" data-toggle="tab">Personal Detail</a></li>
                           <li><a href="#description" data-toggle="tab">Payment Method</a></li>
                        </ul>
                     </div>
                     <div class="tab-content">
                      <!--First tab-->
                        <div class="tab-pane" id="details">
                           <div class="row">
                              <div class="col-sm-12">
                                 <h4 class="info-text"> INCORPORATION OF COMPANY</h4>
                              </div>
                              <div class="col-sm-6 col-md-8">
                                 <!--form code start-->
                  <div id="load-form-message" class="alert " style="display:none">
                  </div>
                  <div class="block-input-info row form-group">
                  <label class="col-md-4">STATE OF INCORPORATION <span class="l-red">*</span> </label>
                  <div class="col-md-8">
                  <select name="state" class="form-control states" id="stateId" required="required">
                  <option value="">Select State</option>
                  <option value="Andhra Pradesh">Andhra Pradesh</option>
                  <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                  <option value="Assam">Assam</option>
                  <option value="Bihar">Bihar</option>
                  <option value="Chandigarh">Chandigarh</option>
                  <option value="Chhattisgarh">Chhattisgarh</option>
                  <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                  <option value="Daman and Diu">Daman and Diu</option>
                  <option value="Delhi">Delhi</option>
                  <option value="Goa">Goa</option>
                  <option value="Gujarat">Gujarat</option>
                  <option value="Haryana">Haryana</option>
                  <option value="Himachal Pradesh">Himachal Pradesh</option>
                  <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                  <option value="Jharkhand">Jharkhand</option>
                  <option value="Karnataka">Karnataka</option>
                  <option value="Kerala">Kerala</option>
                  <option value="Lakshadweep">Lakshadweep</option>
                  <option value="Madhya Pradesh">Madhya Pradesh</option>
                  <option value="Maharashtra">Maharashtra</option>
                  <option value="Manipur">Manipur</option>
                  <option value="Meghalaya">Meghalaya</option>
                  <option value="Mizoram">Mizoram</option>
                  <option value="Nagaland">Nagaland</option>
                  <option value="Odisha">Odisha</option>
                  <option value="Pondicherry">Pondicherry</option>
                  <option value="Punjab">Punjab</option>
                  <option value="Rajasthan">Rajasthan</option>
                  <option value="Sikkim">Sikkim</option>
                  <option value="Tamil Nadu">Tamil Nadu</option>
                  <option value="Telangana">Telangana</option>
                  <option value="Tripura">Tripura</option>
                  <option value="Uttar Pradesh">Uttar Pradesh</option>
                  <option value="Uttarakhand">Uttarakhand</option>
                  <option value="West Bengal">West Bengal</option>
                  </select>
                  <span class="error"></span>
                  </div>
                  </div>
                  <div class="block-input-info row form-group">
                  <label class="col-md-4">TYPE OF COMPANY <span class="l-red">*</span></label>
                  <div class="col-md-8">
                  <select name="companytype" class="form-control select-plan" id="menu-drop" required="required"><option value="">Select company type</option><option value="Private Limited">Private Limited</option><option value="LLP Incorporation">LLP Incorporation</option><option value="OPC Incorporation">OPC Incorporation</option><option value="Charitable company-Section-8">Charitable company-Section-8</option><option value="Public Limited">Public Limited</option></select>           
                  </div>
                  </div>
                  <div class="block-input-info row form-group col-md-4">
                  <label class="">Proposed name of company <span class="l-red">*</span> </label>
                   </div>
                   <div class="block-input-info row  col-md-8">
                    <div class="col-md-6 l-col-prb">
                         <input type="text" name="company-name[]"  class="form-control" placeholder="Name of Company" value="" required="required"> 
                   </div>
                    <div class="col-md-6 l-col-prb">
                         <input type="text" name="company-name[]"  class="form-control" placeholder="Name of Company" value="" > 
                   </div>
                    <div class="col-md-6 l-col-prb">
                         <input type="text" name="company-name[]"  class="form-control" placeholder="Name of Company" value="" > 
                   </div>
                    <div class="col-md-6 l-col-prb">
                         <input type="text" name="company-name[]"  class="form-control" placeholder="Name of Company" value=""> 
                   </div>
                   <div class="col-md-6 l-col-prb">
                         <input type="text" name="company-name[]"  class="form-control" placeholder="Name of Company" value=""> 
                   </div>
                    <div class="col-md-6 l-col-prb">
                         <input type="text" name="company-name[]"  class="form-control" placeholder="Name of Company" value=""> 
                   </div>
                   </div>
                  <div class="block-input-info row form-group" style="">
                  <label class="col-md-4">Number of Directors  </label>
                  <div class="col-md-8">
                  <select id="number-of-directors" name="number-of-directors" class="form-control"><option value="1" selected="">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option></select>
                  </div>
                  </div>
                  <div class="block-input-info row form-group" style="">
                  <label class="col-md-4">No. of Shareholders </label>
                  <div class="col-md-8">
                  <select id="number-of-shareholders" name="number-of-shareholders" class="form-control"><option value="1" selected="">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option></select>
                  </div>
                  </div>
                  <div class="block-input-info row form-group">
                  <label class="col-md-4 ">Main objects of company  </label>
                  <div class="col-md-8">
                   <input id="main-objec" class="form-control" type="text" name="main-object" value="">
                  </div>
                  </div>
                  </div>
                  <div class="col-sm-6 col-md-4" id="order-infor">
                  <div class="block-info-order">
                  <div class="info-order">
                  <div class="form-group">
                  <label>State of incorporation </label>
                  <span class="pull-right incorporation"></span> </div>
                  </div>
                  <div class="info-order">
                  <div class="form-group">
                  <label>Fees for incorporation</label>
                  <span class="pull-right fees">INR 9900  </span><div class="clear"></div>
                  </div>
                  </div>
                  <div class="info-order">
                  <div class="form-group">
                  <label>Possessing time</label>
                  <span class="pull-right">10 working days</span> </div>
                  </div>
                  <div class="info-order">
                  <div class="form-group">
                  <label>Total amount payable</label>
                  <span class="pull-right totalamountpay">INR 9900</span>
                  <input type="hidden" name="totalamount" class="totalamount" value="">
                   </div>
                  </div>
                   <div class="info-order moreoptions">
                  <div class="form-group">
                  <label>Advance to be paid</label>
                  <span class="pull-right"><select name="paidamount" class="form-control validamount" id="menu-drop">
                  <option value="1500">1500</option>
                  <option value="" id="morethan">More than 1500</option>
                  </select>
                  </span> 
                   </div>

                   </div>
                   
                  <div class="form-group ammount" style="display:none;">
                  <input type="text" name="paidamount2" id="numb" placeholder="Enter amount" class="form-control totalam">
                </div>
                  </div>
                  </div>
                  <!-- form code end-->
                  </div>
                  </div>
                  <!--Second tab-->
                  <div class="tab-pane" id="captain">
                  <h4 class="info-text">PERSONEL DETAILS </h4>
                  <div class="row">
                  <!--next code start here-->
                  <div class="col-sm-6 col-md-8 apply-step" id="wrap-left-apply">

                   <div class="block-input-info row form-group col-md-4">
                  <label class="">Name <span class="l-red">*</span> </label>
                   </div>
                   <div class="block-input-info row  col-md-8 l-margin-0">
                    <div class="col-md-9 l-col-prb ">
                        <input id="proposal-name-1" class="form-control username mailfocus" type="text" name="proposal-name" placeholder="Type your name"  value="" required="required">
                   </div>                  
                   </div>



                  <div class="block-input-info row form-group col-md-4">
                  <label class="">Telephone No <span class="l-red">*</span> </label>
                   </div>
                   <div class="block-input-info row  col-md-8 l-margin-0">
                    <div class="col-md-9 l-col-prb ">
                        
                  <input id="proposal-phone-1" class="form-control phoneno mailfocus" type="text" name="telephone-no" placeholder="Type your Telephone Number" maxlength="255" value="" required="required">
                   </div>                  
                   </div>


                   <div class="block-input-info row form-group col-md-4">
                  <label class="">Email Id <span class="l-red">*</span> </label>
                   </div>
                   <div class="block-input-info row  col-md-8 l-margin-0 mailfocus">
                    <div class="col-md-9 l-col-prb ">
                        <input id="proposal-email-1" class="form-control emailcheck" type="email" name="proposal-email" placeholder="Type your Email"  value="" required="required">
                   </div>                  
                   </div>

                   <div class="block-input-info row form-group col-md-4">
                  <label class="">Complete Address </label>
                   </div>
                   <div class="block-input-info row  col-md-8 l-margin-0 mailfocus">
                    <div class="col-md-9 l-col-prb ">
                       <textarea class="form-control" name="address" placeholder="Please Enter your complete Address" rows="6"></textarea>
                   </div>                  
                   </div>


                <!--   <div class="block-input-info row form-group ">
                  <label class="col-md-4">Name <span class="l-red">*</span>
                  </label>
                  <div class="col-md-8">
                  <div class="row l-margin-0">


                  <div class="col-md-9 block-input-info row  l-margin-0">
                  <div class="l-col-prb">
                  <input id="proposal-name-1" class="form-control username mailfocus" type="text" name="proposal-name" placeholder="Type your name"  value="" required="required">
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  <div class="block-input-info row form-group">
                  <label class="col-md-4">Telephone No <span class="l-red">*</span>
                  </label>
                  <div class="col-md-8">
                  <div class="row l-margin-0">
                  <div class="col-md-9 row l-margin-0">
                  <div class="l-col-prb">
                  <input id="proposal-phone-1" class="form-control phoneno mailfocus" type="text" name="telephone-no" placeholder="Type your Telephone Number" maxlength="255" value="" required="required">
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  <div class="block-input-info row form-group mailfocus">
                  <label class="col-md-4">Email Id <span class="l-red">*</span>
                  </label>
                  <div class="col-md-8">
                  <div class="row l-margin-0">
                  <div class="col-md-9 row l-margin-0">
                  <div class="l-col-prb">
                  <input id="proposal-email-1" class="form-control emailcheck mailfocus" type="email" name="proposal-email" placeholder="Type your Email"  value="" required="required">
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                   <div class="block-input-info row form-group mailfocus">
                  <label class="col-md-4">Complete Address
                  </label>
                  <div class="col-md-8">
                  <div class="row l-margin-0">
                  <div class="col-md-9 row l-margin-0">
                  <div class="l-col-prb">
                   <textarea class="form-control" name="address" placeholder="Please Enter your complete Address" rows="6"></textarea>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div> -->

                  </div>
                  <div class="col-sm-6 col-md-4" id="order-infor">
                  <div class="block-info-order">
                  <div class="info-order">
                  <div class="form-group">
                  <label>State of incorporation </label>
                  <span class="pull-right incorporation"></span> </div>
                  </div>
                  <div class="info-order">
                  <div class="form-group">
                  <label>Fees for incorporation</label>
                  <span class="pull-right fees"> 9900  </span><div class="clear"></div>
                  </div>
                  </div>
                  <div class="info-order">
                  <div class="form-group">
                  <label>Possessing time</label>
                  <span class="pull-right">10 working days</span> </div>
                  </div>
                  <div class="info-order">
                  <div class="form-group">
                  <label>Total amount payable</label>
                  <span class="pull-right totalamountpay"> 9900</span> </div>
                  </div>

                  <div class="info-order">
                  <div class="form-group">
                  <label>Advance to be paid</label>
                  <span class="pull-right advancepaid">1500 </span> 
                   </div>
                   </div>
                 
                  
                  </div>
                  </div>
                  <!--End here-->
                  </div>
                  </div>

                  <!--Third tab-->
                  <div class="tab-pane" id="description">
                  <div class="row">
                  <h4 class="info-text"> PAYMENT</h4>
                  <div class="payment_heading">
                  <div class="col-sm-12 text-center">
                    <h3><strong>You have need to pay <span class="advancepaid">1500 </span> rupees amount </strong></h3>
                    <p>Please select the payment method below</p>
                    <div class="col-sm-6">
                 <div class="paymentoption">
                  <input type="radio" id="test" name="payementtype" value="Bank Account" required />
                 <label for="test">Bank Account</label>
                      </div>
                      <div class="left_gateway">
                      <div class="form-group">
                     <p> You can also pay by direct bank account</p>
                 <div class="account_details">
                    <h4>Bank Account Details:-</h4>
                    <p><strong>Account no. 044661900000894</strong></p>
                    <p><strong>IFSC Code:- YESB0000446</strong></p>
                    <p><strong>Beneficiary Name:- Klariti Taxcorp LLP</strong></p>
                  </div>
                    </div>
                  </div>
                </div>
                  <div class="col-sm-6">
                  <div class="paymentoption">
                  <input type="radio" id="test1" name="payementtype" value="Paytm" required />
                 <label for="test1">PAYTM</label>
                      </div>
                    <div class="right_gateway">
                    <div class="form-group">
                 <div class="paytm_qr">
                 <img src="http://klariti.in/wp-content/uploads/2018/02/PaytmQRbanner-page-001.jpg" alt="Smiley face" height="250" width="300">  </div>
               </div>
                </div>
              </div>

                  </div>
                </div>
                  </div>
                  
                  </div>

                  </div>
                  <div class="wizard-footer">
                     <div class="pull-right">
                        <input type='button' class='btn btn-next btn-fill btn-danger btn-wd' name='next' value='Next' />
                       <!--  <input type='button' class='btn btn-finish btn-fill btn-danger btn-wd' name='finish' value='Finish' /> -->
                       <input type="submit" name="submit" value="submit" class="btn btn-finish btn-fill btn-danger btn-wd">
                     </div>
                     <div class="pull-left">
                        <input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='Previous' />
                     </div>
                     <div class="clearfix"></div>
                  </div>
                  </form>
               </div>
            </div>
            <!-- wizard container -->
         </div>
      </div>
      <!-- row -->
   </div>
   <!--  big container -->
</div>

<script>
//display  state name on the base of selected state from dropdown
jQuery(document).ready(function() {
    jQuery("#stateId").change(function() {
        jQuery('.incorporation').html(jQuery(this).val());
    }).change();
});

// if ammount is more than 1500 then add menual price
jQuery(document).ready(function() {
jQuery(".totalam").keyup(function(){
   var totalam = jQuery(".totalam").val();
   jQuery('.advancepaid').text(totalam);
   jQuery('.validamount option[id="morethan"]').val(totalam);
   
});

jQuery(".validamount").on('change', function () {
  var id =  jQuery(this).find('option:selected').attr('id');
if(id == 'morethan'){
jQuery('.ammount').css("display","block");
} else { 
jQuery('.ammount').css("display","none");
 jQuery('.advancepaid').text('1500');
 }
});


});

//if state is punjab kerala nd mp then price change

jQuery(document).ready(function() {
jQuery("#stateId").change(function() {
      var val=jQuery(this).find('option:selected').val();
if(val == 'Punjab'){
jQuery('.fees').html('INR 9900 + 10500');
jQuery('.totalamountpay').text('INR 20400');
jQuery('.totalamount').val('20400');
} else if(val == 'Kerala') {
jQuery('.fees').html( 'INR 9900 + 3200');
jQuery('.totalamountpay').text('INR 13100');
jQuery('.totalamount').val('13100');
} else if(val == 'Madhya Pradesh') {
jQuery('.fees').html('INR 9900 + 8000');
jQuery('.totalamountpay').text('INR 17900');
jQuery('.totalamount').val('17900');
} else{
jQuery('.fees').text('INR 9900');
jQuery('.totalamount').val('9900');
jQuery('.totalamountpay').text('INR 9900');
}
    }).change()
});

//valiation for number
jQuery(document).ready(function () {
  //called when key is pressed in textbox
  jQuery("#numb").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        jQuery("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
jQuery('.select-plan').on('change', function() {
var str = this.value; 
if(str=='Private Limited' || str=='LLP Incorporation' || str=='Charitable company-Section-8' || str=='Public Limited'){
jQuery("#number-of-directors").val("2");
} else {
jQuery("#number-of-directors").val("1");
}
})

});



</script>
<?php
   }
   add_shortcode('startup', 'form_startup');
   ?>