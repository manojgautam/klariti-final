
  <div id="productmenu">
    <div class="submenublock" id="submenu1"><h3>Category 1</h3>
        <ul>
           <li><a href="page.html">Option 1</a></li>
           <li><a href="page.html">Option 2</a></li>
        </ul>
      </div>
        
      <div class="submenublock" id="submenu2"><h3>Category 2</h3>
        <ul>
            <li><a href="page.html">Option 1</a></li>
            <li><a href="page.html">Option 2</a></li>
            <li><a href="page.html">Option 3</a></li>
        </ul>
      </div>
       
   </div>





.submenublock{
    margin: 0px;
    padding: 0px;
}

.submenublock h3{
    margin: 0px;
    padding:5px 10px;
    color: #000099;
}


#productmenu ul{
    list-style-type:none;
    list-style:none;
    margin:0px 0px 0px 20px;
    padding:5px 0px;
}

#productmenu li{
    list-style-type:none;
    list-style:none;
    display: block;
    width:200px;
    padding: 0px;
    color: 
}

#productmenu li a{
    display: block;
}


$(document).ready(function() {
    $('div.submenublock > ul').hide();
    $("div.submenublock > h3").css("background", "url(images/menuarrowdown.gif) no-repeat right bottom");

    $('div.submenublock > h3').click(function() {
        $(this).next().slideToggle('fast', function() {
            //set arrow depending on whether menu is shown or hidden
            if ($(this).is(':hidden')) {
                $(this).prev().css("background", "url(images/menuarrowdown.gif) no-repeat right bottom");
            } else {
                $(this).prev().css("background", "url(images/menuarrowup.gif) no-repeat right bottom");
            }
            return false;
        });

    });
});