-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Aug 23, 2017 at 07:22 AM
-- Server version: 5.6.35-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `klariti_portal`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 NOT NULL,
  `image` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT 'default.jpg',
  `facebook` varchar(20) CHARACTER SET utf8 NOT NULL,
  `google` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `image`, `facebook`, `google`) VALUES
(1, 'Klariti Admin', 'klariti@gmail.com', '7e6ffbf0bf5a5e0736d631666f9b2df9', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 NOT NULL,
  `access_token` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `phone_no` varchar(20) CHARACTER SET utf8 NOT NULL,
  `profile_pic` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT 'default.jpg',
  `facebook_id` varchar(50) NOT NULL,
  `google_id` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=184 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `email`, `password`, `access_token`, `active`, `phone_no`, `profile_pic`, `facebook_id`, `google_id`, `created_at`, `updated_at`) VALUES
(181, 'prashant', 'prashant.techindustan@gmail.com', 'af948f0b6334c5d6e822c9bc8cf24034', '', 1, '9874563965', 'olJBFEw50n9KYQfWtUsa7Ti3CyuGLZ.jpg', '', '', '2017-07-05 20:09:33', '0000-00-00 00:00:00'),
(182, 'chandni Handa', 'chandni.techindustan@gmail.com', '7b1bd46367a22af32b42eb6a79e0b8fb', '', 1, '987456321', '3njPcMlxkmHqB1DrAtgJ9R6pXeYVsu.jpg', '', '', '2017-07-05 20:17:32', '0000-00-00 00:00:00'),
(179, 'Techin Techin', 'aman@techindustan.com', 'b130c1417d51386acdffca98c365face', 'bed040c5c2f1b26c73997904cc9ef54a', 0, '958645235', 'qLdho1SyBCpVAxNYJD5KrkcvHtwXI9.jpg', '467115330321998', '', '2017-07-05 18:52:26', '0000-00-00 00:00:00'),
(178, 'chandni', 'chandni@gmail.com', '7b1bd46367a22af32b42eb6a79e0b8fb', '', 0, '987456325', 'tfIa4JDoGRzdW7ANvlbyq0m2PcO16ThFjMK5n98U.jpg', '', '', '2017-07-05 02:25:46', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
