<div class="container">

<div class="row">
                
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
<?php if($this->session->flashdata('success')==true): ?>
				
				<div class="message_block">
                  <div class="alert alert-success" role="alert">
                      <button class="close" data-dismiss="alert"></button>
                      <strong>Success: </strong><?php echo $this->session->flashdata('success'); ?>
                    </div>
                    </div>
        <?php endif;?>
        <form id="form-personal" role="form" enctype="multipart/form-data" runat="server" autocomplete="off" method="post" action="<?php echo base_url('register'); ?>">

          <?php
        if (validation_errors()) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>
                <b><?php echo 'Alert'; ?>!</b><?php echo validation_errors(); ?>
            </div>

        <?php } ?>    
            <h2>Please Sign Up <small>It's free and always will be.</small></h2>
            <hr class="colorgraph">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="form-group">
                        <input type="text" name="fname" id="first_name" class="form-control input-lg" value="<?php echo set_value('fname');?>" placeholder="First Name" tabindex="1" required>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="form-group">
                        <input type="text" name="lname" id="last_name" class="form-control input-lg" value="<?php echo set_value('lname');?>" placeholder="Last Name" tabindex="2" required>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <input type="text" name="username" id="display_name" class="form-control input-lg" value="<?php echo set_value('username');?>" placeholder="Display Name" tabindex="3">
            </div>
            <div class="form-group">
                <input type="email" name="email" id="email" class="form-control input-lg" value="<?php echo set_value('email');?>" placeholder="Email Address" tabindex="4" required>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="form-group">
                        <input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password" tabindex="5" required>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="form-group">
                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-lg" placeholder="Confirm Password" tabindex="6">
                    </div>
                </div>
            </div>
		<div class="form-group">
                        <select id="curso" name="usertype"  class="form-control input-lg"  tabindex="2" required>
                            <option value="">Select User Type</option>
                            <option value="standard">Standard User</option>
                            <option value="accountant">Accountant</option>
                        </select>
					</div>
       

            <div class="row">
                <div class="col-xs-4 col-sm-3 col-md-3">
                    <span class="button-checkbox">
                        <button type="button" class="btn" data-color="info" tabindex="7">I Agree</button>
                        <input type="checkbox" name="t_and_c" id="t_and_c" class="hidden" value="1">
                    </span>
                </div>
                <div class="col-xs-8 col-sm-9 col-md-9">
                     By clicking <strong class="label label-primary">Register</strong>, you agree to the <a href="#" data-toggle="modal" data-target="#t_and_c_m">Terms and Conditions</a> set out by this site, including our Cookie Use.
                </div>
            </div>
            
            <hr class="colorgraph">
            <div class="row">
                <div class="col-xs-12 col-md-6"><input type="submit" name="submit" value="Register" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
	<div class="col-xs-12 col-md-6"><a href="<?php echo base_url('login'); ?>" class="btn btn-success btn-block btn-lg">Sign In</a></div>
            </div>
        </form>
    </div>
</div>
</div>