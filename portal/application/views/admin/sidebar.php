<body class="fixed-header dashboard sidebar-visible menu-pin">
    <!-- BEGIN SIDEBPANEL-->
    <nav class="page-sidebar" data-pages="sidebar">

      <!-- BEGIN SIDEBAR MENU HEADER-->
      <div class="sidebar-header">
        <img src="<?php echo base_url('assets/admin/img/logo_white.png')?>" alt="logo" class="brand" data-src="<?php echo base_url('assets/admin/img/logo_white.png')?>" data-src-retina="<?php echo base_url('assets/admin/img/logo_white_2x.png')?>" width="78" height="22">
        <div class="sidebar-header-controls">
       
          <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar"><i class="fa fs-12"></i>
          </button>
        </div>
      </div>
      <!-- END SIDEBAR MENU HEADER-->
      <!-- START SIDEBAR MENU -->
      <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
          <li class="m-t-30 ">
            <a href="#" class="detailed">
              <span class="title">Dashboard</span>             
            </a>
            <span class="bg-success icon-thumbnail"><i class="pg-home"></i></span>
          </li>
          
          <li class="">
            <a href="<?php echo base_url('admin/user'); ?>" class="detailed">
              <span class="title">Users</span>              
            </a>
            <span class="icon-thumbnail">U</span>
          </li>
          <li class="">
            <a href="<?php echo base_url('admin/accountant'); ?>"><span class="title">Accountant</span></a>
            <span class="icon-thumbnail"><i class="pg-social"></i></span>
          </li>
          <li>
            <a href="<?php echo base_url('admin/project'); ?>"><span class="title">Projects</span>
            <span class=" arrow"></span></a>
            <span class="icon-thumbnail">P</span>
            <ul class="sub-menu">
              <li class="">
                <a href="#">Active</a>
                <span class="icon-thumbnail">A</span>
              </li>
              <li class="">
                <a href="#">Pending</a>
                <span class="icon-thumbnail">P</span>
              </li>
              <li class="">
                <a href="#">Completed</a>
                <span class="icon-thumbnail">C</span>
              </li>
              <li class="">
                <a href="#">Bids</a>
                <span class="icon-thumbnail">B</span>
              </li>
            
            </ul>
          </li>
        
          <li>
            <a href="javascript:;"><span class="title">Payment</span>
            <span class=" arrow"></span></a>
            <span class="icon-thumbnail"><i class="pg-layouts2"></i></span>
            <ul class="sub-menu">
              <li class="">
                <a href="#">Default</a>
                <span class="icon-thumbnail">dl</span>
              </li>
              <li class="">
                <a href="#">Secondary</a>
                <span class="icon-thumbnail">sl</span>
              </li>
              <li class="">
                <a href="#">Boxed</a>
                <span class="icon-thumbnail">bl</span>
              </li>
              <li class="">
                <a href="#">Horizontal Menu</a>
                <span class="icon-thumbnail">hm</span>
              </li>
              <li class="">
                <a href="#">RTL</a>
                <span class="icon-thumbnail">rl</span>
              </li>
              <li class="">
                <a href="#">Columns</a>
                <span class="icon-thumbnail">cl</span>
              </li>
            </ul>
          </li>
            <li class="">
            <a href="<?php echo base_url('admin/categories'); ?>">
              <span class="title">Categories</span>
            </a>
            <span class="icon-thumbnail">Pc</span>
          </li>
             
             <li>
            <a href="javascript:;"><span class="title">Cost Slabs</span>
            <span class=" arrow"></span></a>
            <span class="icon-thumbnail">Cs</span>
            <ul class="sub-menu">
              <li class="">
                <a href="<?php echo base_url('admin/cost'); ?>">Add Cost</a>
                <span class="icon-thumbnail">Ac</span>
              </li>
              <li class="">
                <a href="<?php echo base_url('admin/cost/listcost'); ?>">List Cost</a>
                <span class="icon-thumbnail">Lc</span>
              </li>
      
            </ul>
          </li>

        </ul>
        <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->
    </nav>
    <!-- END SIDEBAR -->
    <!-- END SIDEBPANEL-->
       <!-- START PAGE-CONTAINER -->
    <div class="page-container ">
      <!-- START HEADER -->
      <div class="header ">
        <!-- START MOBILE CONTROLS -->
        <div class="container-fluid relative">
          <!-- LEFT SIDE -->
          <div class="pull-left full-height visible-sm visible-xs">
            <!-- START ACTION BAR -->
            <div class="header-inner">
              <a href="#" class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5" data-toggle="sidebar">
                <span class="icon-set menu-hambuger"></span>
              </a>
            </div>
            <!-- END ACTION BAR -->
          </div>
          <div class="pull-center hidden-md hidden-lg">
            <div class="header-inner">
              <div class="brand inline">
                <img src="<?php echo base_url('assets/admin/img/logo.png')?>" alt="logo" data-src="<?php echo base_url('assets/admin/img/logo.png')?>" data-src-retina="<?php echo base_url('assets/admin/img/logo_2x.png')?>" width="78" height="22">
              </div>
            </div>
          </div>
          <!-- RIGHT SIDE -->
          <div class="pull-right full-height visible-sm visible-xs">
            <!-- START ACTION BAR -->
            <div class="header-inner">
              <a href="#" class="btn-link visible-sm-inline-block visible-xs-inline-block" data-toggle="quickview" data-toggle-element="#quickview">
                <span class="icon-set menu-hambuger-plus"></span>
              </a>
            </div>
            <!-- END ACTION BAR -->
          </div>
        </div>
        <!-- END MOBILE CONTROLS -->
        <div class=" pull-left sm-table hidden-xs hidden-sm">
          <div class="header-inner">
            <div class="brand inline">
              <img src="<?php echo base_url('assets/admin/img/logo.png')?>" alt="logo" data-src="<?php echo base_url('assets/admin/img/logo.png')?>" data-src-retina="<?php echo base_url('assets/admin/img/logo_2x.png')?>" width="78" height="22">
            </div>
</div>
        </div>
        <div class=" pull-right">
          <div class="header-inner">
            <a href="#" class="btn-link icon-set menu-hambuger-plus m-l-20 sm-no-margin hidden-sm hidden-xs" data-toggle="quickview" data-toggle-element="#quickview"></a>
          </div>
        </div>
        <div class=" pull-right">
          <!-- START User Info-->
          <div class="visible-lg visible-md m-t-10">
            <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
              <span class="semi-bold">David</span> <span class="text-master">Nest</span>
            </div>
            <div class="dropdown pull-right">
              <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="thumbnail-wrapper d32 circular inline m-t-5">
                <img src="<?php echo base_url('assets/admin/img/profiles/avatar.jpg')?>" alt="" data-src="<?php echo base_url('assets/admin/img/profiles/avatar.jpg')?>" data-src-retina="<?php echo base_url('assets/admin/img/profiles/avatar_small2x.jpg')?>" width="32" height="32">
            </span>
              </button>
              <ul class="dropdown-menu profile-dropdown" role="menu">
                <li><a href="#"><i class="pg-settings_small"></i> Settings</a>
                </li>
                <li><a href="#"><i class="pg-outdent"></i> Feedback</a>
                </li>
                <li><a href="#"><i class="pg-signals"></i> Help</a>
                </li>
                <li class="bg-master-lighter">
                  <a href="#" class="clearfix">
                    <span class="pull-left">Logout</span>
                    <span class="pull-right"><i class="pg-power"></i></span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <!-- END User Info-->
        </div>
      </div>
      <!-- END HEADER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">