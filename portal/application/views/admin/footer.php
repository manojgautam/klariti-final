  </div>
  </div>
   <!-- BEGIN VENDOR JS -->
    <script src="<?php echo base_url('assets/admin/plugins/pace/pace.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/plugins/jquery/jquery-1.11.1.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/plugins/modernizr.custom.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/plugins/jquery-ui/jquery-ui.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/plugins/bootstrapv3/js/bootstrap.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/plugins/jquery/jquery-easy.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/plugins/jquery-unveil/jquery.unveil.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/plugins/jquery-bez/jquery.bez.min.js')?>"></script>
    <script src="<?php echo base_url('assets/admin/plugins/jquery-ios-list/jquery.ioslist.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/plugins/jquery-actual/jquery.actual.min.js')?>"></script>
    <script src="<?php echo base_url('assets/admin/plugins/jquery-scrollbar/jquery.scrollbar.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/admin/plugins/select2/js/select2.full.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/admin/plugins/classie/classie.js')?>"></script>
    <script src="<?php echo base_url('assets/admin/plugins/switchery/js/switchery.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/plugins/nvd3/lib/d3.v3.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/plugins/nvd3/nv.d3.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/plugins/nvd3/src/utils.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/plugins/nvd3/src/tooltip.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/plugins/nvd3/src/interactiveLayer.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/plugins/nvd3/src/models/axis.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/plugins/nvd3/src/models/line.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/plugins/nvd3/src/models/lineWithFocusChart.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/plugins/mapplic/js/hammer.js')?>"></script>
    <script src="<?php echo base_url('assets/admin/plugins/summernote/js/summernote.min.js')?>"></script
    <script src="<?php echo base_url('assets/admin/plugins/mapplic/js/jquery.mousewheel.js')?>"></script>
    <script src="<?php echo base_url('assets/admin/plugins/mapplic/js/mapplic.js')?>"></script>
  // <script src="<?php //echo base_url('assets/admin/plugins/rickshaw/rickshaw.min.js')?>"></script>
    <script src="<?php echo base_url('assets/admin/plugins/jquery-metrojs/MetroJs.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/plugins/jquery-sparkline/jquery.sparkline.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/plugins/skycons/skycons.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')?>" type="text/javascript"></script>

    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="<?php echo base_url('assets/admin/js/pages.min.js')?>"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->

 <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?php //echo base_url('assets/admin/js/form_elements.js')?>" type="text/javascript"></script>
   // <script src="<?php //echo base_url('assets/admin/js/dashboard.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/js/scripts.js')?>" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->
<!-- ajax for category add -->
<script>
$(document).ready(function () {
 $("#addcat").click(function (e) {

  var name = $('#cate').val();
    var parent = 0;
    if(name.trim() != "" ){
    $.ajax({
               url:'<?php echo base_url();?>admin/categories/addcategoryajax',
               type:'POST',
                data:{name: name,parent: parent},
               success:function(data){
                 //console.log(data);
                 $('.msg').text("Category Added Successfully");
                 $('.msg').show();
                  $('.ajaxcateg').html(data);
         $("#categrysel").select2();
        
               }
               
           });
           
           } else {
       
                      
           return false;
           
           }
           });

});
</script>
<script>
/* Formatting function for row details - modify as you need */
function format ( d ) {
    // `d` is the original data object for the row
    var html =  '<table class="table table-hover demo-table-search table-responsive-block subcat" >';
  html+= '<tr>'+
            '<th>Title:</th>'+
            '<th>Project Count:</th>'+
            '<th>Status:</th>'+
            '<th>Action:</th>'+
        '</tr>';
    $.each(d, function(i,v) {
        html +=       
        '<tr>'+            
            '<td>'+ v.name +'</td>'+
            '<td>'+ v.project_count +'</td>'+
             '<td>'+ v.status +'</td>'+
              '<td><a  href="<?php echo base_url('');?>admin/categories/subcategoriesdelete/'+ v.id +'" class="btn btn-danger  m-b-10 deleterecord"><i class="fa fa-trash-o"></i></a><a  href="javascript:void(0)" data-toggle="modal" data-target="#modalSlideUpsub_'+ v.id +'" class="btn  m-b-10"><i class="fa fa-edit"></i></a></td>'+
        '</tr>';
     });
    html += '</table>';
    
    return html;
}
 
$(document).ready(function() {
    var table = $('#example').DataTable();
     
    // Add event listener for opening and closing details
    $('#example tbody').on('click', '.showsubcategory', function () {
    var index = $(this).attr("data-index");
    console.log(categories, index, $(this)[0]);
    var subcategories = categories[index].subcategories;
   var subcatcount = subcategories.length;
     var getid = $(this).attr('getid');
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            if(subcatcount){
            row.child( format(subcategories) ).show();
            tr.addClass('shown');
            }
        }
    } );
} );
</script>


<!--select category and subcategory-->
<script>
$(".costcategory").change(function(){
var id =  $(this).val();
if(id != ''){
  $.ajax({
               url:'<?php echo base_url();?>admin/cost/getsubcatbyajax',
               type:'POST',
                data:{id: id},
               success:function(data){
                 //console.log(data);
                 $('.subcategory').html(data);
                 $('#subcurso').select2();
               }
               
           });
           }else{
           
            $('.subcategory').html('<select id="subcurso" name="subcategory"  class="form-control input-lg subcat"  tabindex="2" required><option value="">Please Select Subcategory </option></select>');
           }
 
});

</script>
<!--end category and subcategory -->
<!--Editor -->
<script>


        var myCustomTemplates = {
                "font-styles": function(locale) {
                    return '<li class="dropdown">' + '<a data-toggle="dropdown" class="btn btn-default dropdown-toggle ">' + '<span class="editor-icon editor-icon-headline"></span>' + '<span class="current-font">Normal</span>' + '<b class="caret"></b>' + '</a>' + '<ul class="dropdown-menu">' + '<li><a tabindex="-1" data-wysihtml5-command-value="p" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">Normal</a></li>' + '<li><a tabindex="-1" data-wysihtml5-command-value="h1" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">1</a></li>' + '<li><a tabindex="-1" data-wysihtml5-command-value="h2" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">2</a></li>' + '<li><a tabindex="-1" data-wysihtml5-command-value="h3" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">3</a></li>' + '<li><a tabindex="-1" data-wysihtml5-command-value="h4" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">4</a></li>' + '<li><a tabindex="-1" data-wysihtml5-command-value="h5" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">5</a></li>' + '<li><a tabindex="-1" data-wysihtml5-command-value="h6" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">6</a></li>' + '</ul>' + '</li>';
                },
                emphasis: function(locale) {
                    return '<li>' + '<div class="btn-group">' + '<a tabindex="-1" title="CTRL+B" data-wysihtml5-command="bold" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-bold"></i></a>' + '<a tabindex="-1" title="CTRL+I" data-wysihtml5-command="italic" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-italic"></i></a>' + '<a tabindex="-1" title="CTRL+U" data-wysihtml5-command="underline" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-underline"></i></a>' + '</div>' + '</li>';
                },
                blockquote: function(locale) {
                    return '<li>' + '<a tabindex="-1" data-wysihtml5-display-format-name="false" data-wysihtml5-command-value="blockquote" data-wysihtml5-command="formatBlock" class="btn  btn-default" href="javascript:;" unselectable="on">' + '<i class="editor-icon editor-icon-quote"></i>' + '</a>' + '</li>'
                },
                lists: function(locale) {
                    return '<li>' + '<div class="btn-group">' + '<a tabindex="-1" title="Unordered list" data-wysihtml5-command="insertUnorderedList" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-ul"></i></a>' + '<a tabindex="-1" title="Ordered list" data-wysihtml5-command="insertOrderedList" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-ol"></i></a>' + '<a tabindex="-1" title="Outdent" data-wysihtml5-command="Outdent" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-outdent"></i></a>' + '<a tabindex="-1" title="Indent" data-wysihtml5-command="Indent" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-indent"></i></a>' + '</div>' + '</li>'
                },
                image: function(locale) {
                    return '<li>' + '<div class="bootstrap-wysihtml5-insert-image-modal modal fade">' + '<div class="modal-dialog ">' + '<div class="modal-content">' + '<div class="modal-header">' + '<a data-dismiss="modal" class="close">Ã—</a>' + '<h3>Insert image</h3>' + '</div>' + '<div class="modal-body">' + '<input class="bootstrap-wysihtml5-insert-image-url form-control" value="http://">' + '</div>' + '<div class="modal-footer">' + '<a data-dismiss="modal" class="btn btn-default">Cancel</a>' + '<a data-dismiss="modal" class="btn btn-primary">Insert image</a>' + '</div>' + '</div>' + '</div>' + '</div>' + '<a tabindex="-1" title="Insert image" data-wysihtml5-command="insertImage" class="btn  btn-default" href="javascript:;" unselectable="on">' + '<i class="editor-icon editor-icon-image"></i>' + '</a>' + '</li>'
                },
                link: function(locale) {
                    return '<li>' + '<div class="bootstrap-wysihtml5-insert-link-modal modal fade">' + '<div class="modal-dialog ">' + '<div class="modal-content">' + '<div class="modal-header">' + '<a data-dismiss="modal" class="close">Ã—</a>' + '<h3>Insert link</h3>' + '</div>' + '<div class="modal-body">' + '<input class="bootstrap-wysihtml5-insert-link-url form-control" value="http://">' + '<label class="checkbox"> <input type="checkbox" checked="" class="bootstrap-wysihtml5-insert-link-target">Open link in new window</label>' + '</div>' + '<div class="modal-footer">' + '<a data-dismiss="modal" class="btn btn-default">Cancel</a>' + '<a data-dismiss="modal" class="btn btn-primary" href="#">Insert link</a>' + '</div>' + '</div>' + '</div>' + '</div>' + '<a tabindex="-1" title="Insert link" data-wysihtml5-command="createLink" class="btn  btn-default" href="javascript:;" unselectable="on">' + '<i class="editor-icon editor-icon-link"></i>' + '</a>' + '</li>'
                },
                html: function(locale) {
                    return '<li>' + '<div class="btn-group">' + '<a tabindex="-1" title="Edit HTML" data-wysihtml5-action="change_view" class="btn  btn-default" href="javascript:;" unselectable="on">' + '<i class="editor-icon editor-icon-html"></i>' + '</a>' + '</div>' + '</li>'
                }
            }
   


        $('#summernote').summernote({
            height: 200,
            onfocus: function(e) {
                $('body').addClass('overlay-disabled');
            },
            onblur: function(e) {
                $('body').removeClass('overlay-disabled');
            }
        });


$('.costslab').on('click', function() {
    var data = $('.note-editable').html();
$('.costdesc').val(data);

});
</script>

  </body>
</html>