  <!-- START CONTAINER FLUID -->

  
  <script>
  	var categories = <?php echo json_encode($categorlist, JSON_HEX_TAG); ?>
  </script>
          <div class="container-fluid container-fixed-lg bg-white">
            <!-- START PANEL -->
            <div class="panel panel-transparent">
              <div class="panel-heading">
                <div class="panel-title">Pages Default Tables Style
                </div>
                <a href="javascript:void(0)" data-toggle="modal" data-target="#modalSlideUpcate" class="btn btn-primary btn-cons pull-right"><i class="fa fa-plus"></i> Manage Categories</a>
                  
                <div class="clearfix"></div>
              </div>
              <div class="panel-body">
                <table class="table table-hover demo-table-search table-responsive-block" id="example">
                  <thead>
                    <tr>
                      <th>Title</th>
                      <th>Project Count</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  
                  <tbody>
                   <?php for($i = 0; $i < count($categorlist); $i++){ $cat = $categorlist[$i]; ?>
                    <tr>
                      <td data-index="<?php echo $i; ?>" class="v-align-middle semi-bold  showsubcategory" getid="<?php echo $cat["id"]; ?>">
                        <p><?php echo $cat["name"]; ?></p>
                      </td>                     
                      <td class="v-align-middle">
                        <p><?php echo $cat["project_count"]; ?></p>
                      </td>
                      <td class="v-align-middle">
                        <p><?php echo $cat["status"]; ?></p>
                      </td>
                      <td class="v-align-middle">
                        <p><a  href="<?php echo base_url('admin/categories/categoriesdelete/'.$cat['id']);?>" class="btn btn-danger  m-b-10 deleterecord"><i class="fa fa-trash-o"></i></a></p>
                         <p><a href="javascript:void(0)" data-toggle="modal" data-target="#modalSlideUpsub_<?php echo $cat["id"]; ?>" class="btn m-b-10 updaterecorde"><i class="fa fa-edit"></i></a></p>
                      </td>
                    </tr>
                             <!-- Modal for update category -->
          <div class="modal fade slide-up disable-scroll" id="modalSlideUpsub_<?php echo $cat["id"]; ?>" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog ">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Update<span class="semi-bold">Categories</span></h5>
                   
                  </div>
                  <div class="modal-body">
                 <div class="panel panel-default">
                 
                  <div class="panel-body">
                    <form method='post' action="<?php echo base_url();?>admin/categories/updatecategory/<?php echo $cat["id"]; ?>" id="userform">
                      <div class="row">
                       
           

              <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Category name</label>
                            <input type="text" name="csubname" class="form-control" value="<?php echo $cat["name"]; ?>" required>
                          </div>
                        </div>
                        
                      </div>
                      
                      <input type="submit" name="submit" class="btn btn-primary" value="Add Category" >
                    </form>

                  </div>
                </div>
                <!-- END PANEL -->
                  </div>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
          </div>
          <!-- /.modal-dialog -->
          </div>
          
          
          
                      <?php foreach($cat['subcategories'] as $c){ ?>
                                       <!-- Modal for update category -->
          <div class="modal fade slide-up disable-scroll" id="modalSlideUpsub_<?php echo $c["id"]; ?>" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog ">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Update<span class="semi-bold">Categories</span></h5>
                   
                  </div>
                  <div class="modal-body">
                 <div class="panel panel-default">
                 
                  <div class="panel-body">
                    <form method='post' action="<?php echo base_url();?>admin/categories/updatesubcategory/<?php echo $c["id"]; ?>" id="userform">
                      <div class="row">
                       
           

              <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Category name</label>
                            <input type="text" name="csubname" class="form-control" value="<?php echo $c["name"]; ?>" required>
                          </div>
                        </div>
                        
                      </div>
                      
                      <input type="submit" name="submit" class="btn btn-primary" value="Add Category" >
                    </form>

                  </div>
                </div>
                <!-- END PANEL -->
                  </div>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
          </div>
          <!-- /.modal-dialog -->
          </div>
          <?php } ?>
          
          
          
         <?php } ?>

                  </tbody>
                </table>
              </div>
            </div>
            <!-- END PANEL -->
          </div>
          <!-- END CONTAINER FLUID -->
          
          
          
                    <!-- Modal for category and sub-category-->
          <div class="modal fade slide-up disable-scroll" id="modalSlideUpcate" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog ">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                <div class="wel">
                  <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                     <ul class="nav nav-tabs">
      <li class="active"><a href="#home" data-toggle="tab">Add Category</a></li>
      <li><a href="#profile" data-toggle="tab">Add Sub category</a></li>
    </ul>
                  </div>
                  <div class="modal-body">
                 <div class="panel panel-default">
                 
                  <div class="panel-body">
                      <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
        <div class="msg success alert-success"></div>
             <form method='post' action="" id="userform">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Category name</label>
                            <input type="text" name="cname" class="form-control" id="cate" required>
                          </div>
                        </div>
                        
                      </div>
                      
                      <input type="button" name="submit" class="btn btn-primary" id="addcat" value="Add Category" >
                    </form>
   
      </div>
      <div class="tab-pane fade" id="profile">
    <form method='post' action="<?php echo base_url();?>admin/categories/addsubcategory/" id="userform">
                      <div class="row">
                       
                <div class="col-sm-12">
                          <div class="form-group form-group-default form-group-default-select2 required">
                          <label class="">Select Category</label>
                            <div class="ajaxcateg">
                        <select name="category" class="full-width" id="categrysel" data-placeholder="Select Country" data-init-plugin="select2">    
                    <?php if($categorlist): foreach($categorlist as $cat): ?>
                    <option value="<?php echo $cat["id"]; ?>"> <?php echo $cat["name"]; ?> </option>
                    <?php endforeach; else:?>
				<p>No Category Found</p>
				<?php endif; ?>
                 </select></div>
                 </div>
                 </div>

              <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Sub Category name</label>
                            <input type="text" name="csubname" class="form-control" required>
                          </div>
                        </div>
                        
                      </div>
                      
                      <input type="submit" name="submit" class="btn btn-primary" value="Add Category" >
                    </form>

        
      </div>
                  </div>
                </div>
                <!-- END PANEL -->
                  </div>
                </div>
              </div></div>
              <!-- /.modal-content -->
            </div>
          </div></div>
          <!-- /.modal-dialog -->
          
          
          
          
   