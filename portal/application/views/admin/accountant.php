    <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg bg-white">
            <!-- START PANEL -->
            <div class="panel panel-transparent">
              <div class="panel-heading">
                <div class="panel-title">Accountant List
                </div>
                
                <div class="clearfix"></div>
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                  <table class="table table-hover" id="basicTable">
                    <thead>
                      <tr>
                        <th style="width:1%">
                          <button class="btn"><i class="pg-trash"></i>
                          </button>
                        </th>
                        <th style="width:20%">Name</th>
                        <th style="width:20%">Email</th>
                        <th style="width:29%">Verification</th>
                        <th style="width:15%">Status</th>
                        <th style="width:15%">Last Update</th>
                      </tr>
                    </thead>
                    <tbody>
<?php foreach($allaccountant as $accountant){ ?>
                      <tr>
                        <td class="v-align-middle">
                          <div class="checkbox ">
                            <input type="checkbox" value="3" id="checkbox1">
                            <label for="checkbox1"></label>
                          </div>
                        </td>
                        <td class="v-align-middle ">
                          <p><?php echo $accountant->username; ?></p>
                        </td>
                        <td class="v-align-middle">
                          <p><?php echo $accountant->email; ?></p>
                        </td>
                        <td class="v-align-middle">
                          <p><?php echo $accountant->verified; ?></p>
                        </td>
                        <td class="v-align-middle">
                          <p>Public</p>
                        </td>
                        <td class="v-align-middle">
                          <p>April 13,2014 10:13</p>
                        </td>
                      </tr>
<?php } ?>
                     
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- END PANEL -->
          </div>
          <!-- END CONTAINER FLUID -->