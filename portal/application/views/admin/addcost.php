
          <!-- START CONTAINER FLUID -->
			<div class="container-fluid container-fixed-lg">
				<div class="row">
				 <div class="col-lg-12 col-md-12">
				  
                         <!-- START PANEL -->
                          <div class="panel panel-transparent">
           
                    
                    <div class="clearfix"></div>
               
                <?php if($this->session->flashdata('success')==true): ?>
				 <div class="message_block">
                <div class="alert alert-success" role="alert">
                      <button class="close" data-dismiss="alert"></button>
                      <strong>Success: </strong><?php echo $this->session->flashdata('success'); ?>
                    </div>
                    </div>
        <?php endif;?>
                <div class="panel-body">
                   
              <form method='post' action="<?php base_url('admin/cost/');?>" id="userform">
			 <?php
        if (validation_errors()) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>
                <b><?php echo 'Alert'; ?>!</b><?php echo validation_errors(); ?>
            </div>

        <?php } ?>		
	  
                      <div class="row">
                       
   <div class="col-md-6 col-sm-6">
	         <div class="form-group form-group-default form-group-default-select2 required">
	         <label class="">Select Category</label>
                 <select id="curso" name="category"  class="full-width input-lg costcategory"  data-init-plugin="select2"  required>
                 <option value="">Please Select Category </option>
                    <?php foreach($categorlist as $cat){ ?>
                    <option value="<?php echo $cat["id"]; ?>"> <?php echo $cat["name"]; ?> </option>
                    <?php } ?>
                 </select>
                 </div>
		</div>
		  <div class="col-md-6 col-sm-6 ">
	         <div class="form-group form-group-default form-group-default-select2 subcategory required">
	         <label class="">Select Sub Category</label>
                 <select id="subcurso" name="subcategory" class="full-width input-lg subcat" data-init-plugin="select2" required>                   
                    <option value="">Please Select Subcategory </option>                    
                 </select>
                 </div>
		</div>
                        
                        <div class="col-sm-6">
                          <div class="form-group form-group-default required">
                            <label>Name</label>
                            <input type="text" name="costname" class="form-control" required>
                          </div>
                        </div>
                                                
                         <div class="col-sm-16">
                          <div class="form-group form-group-default required">
                            <label>Cost</label>
                            <input type="text" name="cost" class="form-control" required>
                          </div>
                        </div>
                        
                        
                          <div class="panel panel-default">
       
              <div class="panel-body no-scroll ">
                <h5>Description</h5>
                <div class="summernote-wrapper">
                  <div id="summernote"></div>
                   <input type="hidden" name="costdesc" class="form-control costdesc" required>
                </div>
              </div>
            </div>
          

                        
                      </div>
                      
                      <input type="submit" name="submit" class="btn btn-primary costslab" value="Add Cost Slab" >
                    </form>

                </div>
                </div>
                </div>
				 </div>
                <!-- END PANEL -->
			</div>
			
			                     
			
