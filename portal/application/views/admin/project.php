  <!-- START CONTAINER FLUID -->

          <div class="container-fluid container-fixed-lg bg-white">
            <!-- START PANEL -->
            <div class="panel panel-transparent">
              <div class="panel-heading">
                <div class="panel-title">All Projects
                </div>
         
                  
                <div class="clearfix"></div>
              </div>
              <div class="panel-body">
                <table class="table table-hover demo-table-search table-responsive-block" id="example">
                  <thead>
                    <tr>
                      <th>Title</th>
                      <th>Project Description</th>
                      <th>User</th>
                      <th>Category</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  
                  <tbody>
                 
                   <?php foreach($allproject as $project) {?>
                    <tr>
                      <td  class="v-align-middle semi-bold  showsubcategory">
                        <p><?php echo $project->title; ?></p>
                      </td>                     
                      <td class="v-align-middle">
                        <p><?php echo $project->description; ?></p>
                      </td>
                      <td class="v-align-middle">
                        <p><?php echo $project->username; ?></p>
                      </td>
                      <td class="v-align-middle">
                        <p><?php echo $project->catname; ?> => <?php echo $project->subcatname; ?></p>
                      </td>
                      <td class="v-align-middle">
                        <p><?php echo $project->status; ?></p>
                      </td>
                      <td class="v-align-middle">
                        <p><a  href="<?php echo base_url('admin/project/projectdelete/'.$project->id);?>" class="btn btn-danger  m-b-10 deleterecord"><i class="fa fa-trash-o"></i></a></p>
                       
                      </td>
                    </tr>
                    <?php } ?>
                     </tbody>
                   </table>
                   </div>
                   </div>
                   </div>
          
          
         