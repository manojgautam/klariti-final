  <!-- START CONTAINER FLUID -->

  
          <div class="container-fluid container-fixed-lg bg-white">
            <!-- START PANEL -->
            <div class="panel panel-transparent">
              <div class="panel-heading">
                <div class="panel-title">Pages Default Tables Style
                </div>
                <a href="javascript:void(0)" data-toggle="modal" data-target="#modalSlideUpcate" class="btn btn-primary btn-cons pull-right"><i class="fa fa-plus"></i> Manage Cost Slab</a>
                  
                <div class="clearfix"></div>
              </div>
              <div class="panel-body">
                <table class="table table-hover demo-table-search table-responsive-block" id="example">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Description</th>
                      <th>Cost</th>
                      <th>Category</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  
                  <tbody>
                  <?php foreach($costslab as $cost){ ?>
                    <tr>
                      <td class="v-align-middle semi-bold" >
                        <p><?php echo $cost->slabname; ?></p>
                      </td>                     
                      <td class="v-align-middle">
                        <p><?php echo $cost->description; ?></p>
                      </td>
                      <td class="v-align-middle">
                        <p><?php echo $cost->cost; ?></p>
                      </td>
                      <td class="v-align-middle">
                       <p><?php echo $cost->catname; ?> => <?php echo $cost->subcatname; ?></p>
                      </td>
                      <td class="v-align-middle">
                       <p></p>
                      </td>
                    </tr>
                    <?php } ?>

                  </tbody>
                </table>
              </div>
            </div>
            <!-- END PANEL -->
          </div>
          <!-- END CONTAINER FLUID -->
          
          
          
                    <!-- Modal for cost slab-->
          <div class="modal fade slide-up disable-scroll" id="modalSlideUpcate" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog ">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                <div class="wel">
                  <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                 
                  </div>
                  <div class="modal-body">
                 <div class="panel panel-default">
                 
                  <div class="panel-body">
     
        <div class="msg success alert-success"></div>
         
      
<form method='post' action="<?php base_url('admin/cost/');?>" id="userform">
                      <div class="row">
                       
   <div class="col-md-12 col-sm-12">
	         <div class="form-group form-group-default form-group-default-select2 required">
	         <label class="">Select Category</label>
                 <select id="curso" name="category"  class="full-width input-lg costcategory"  data-init-plugin="select2"  required>
                 <option value="">Please Select Category </option>
                    <?php foreach($categorlist as $cat){ ?>
                    <option value="<?php echo $cat["id"]; ?>"> <?php echo $cat["name"]; ?> </option>
                    <?php } ?>
                 </select>
                 </div>
		</div>
		  <div class="col-md-12 col-sm-12 ">
	         <div class="form-group form-group-default form-group-default-select2 subcategory required">
	         <label class="">Select Sub Category</label>
                 <select id="curso" name="subcategory" class="full-width input-lg subcat" data-init-plugin="select2" required>                   
                    <option value="">Please Select Subcategory </option>                    
                 </select>
                 </div>
		</div>
                        
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Name</label>
                            <input type="text" name="costname" class="form-control" required>
                          </div>
                        </div>
                        
                           <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Description</label>
                            <input type="text" name="costdesc" class="form-control" required>
                          </div>
                        </div>
                        
                           <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Cost</label>
                            <input type="text" name="cost" class="form-control" required>
                          </div>
                        </div>
                        
                      </div>
                      
                      <input type="submit" name="submit" class="btn btn-primary" value="Add Cost Slab" >
                    </form>


       
                </div>
                <!-- END PANEL -->
                  </div>
                </div>
              </div></div>
              <!-- /.modal-content -->
            </div>
          </div></div>
          <!-- /.modal-dialog -->
          
          
          
          
   