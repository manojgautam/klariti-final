<div class="container">
<button>Edit Project Details</button>
<h1><?php echo $projectdetail[0]->title; ?></h1>
<p><?php echo $projectdetail[0]->description; ?></p>


<div class="add_documents">
 <h3>Add Your Documents</h3>    
                         <form action="<?php echo base_url('user/project/fileupload');?>" enctype="multipart/form-data" method="post" class="dropzone addfile">
             <div class="fallback">
              <input name="file[]" type="file" multiple />
              
		<input type="hidden" name="projectid" value="<?php echo $projectdetail[0]->id; ?>">
             </div>
          </form>
</div>


          
</div>
<section class="documents_project">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="col-lg-12">
          <h6 class="description">Documents</h6>
          <div class="row pt-md">
          <?php foreach($docsdetail as $docs) { ?>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
              <div class="img-box">
              
                <?php $array_formats = array("docx", "xsl", "pdf", "txt"); ?>
                <?php  if($docs->extension == 'jpg' || $docs->extension == 'jpeg'  || $docs->extension == 'png') { ?>                     
                  <img src="<?php echo base_url('');?>assets/timthumb.php?src=/<?php echo $docs->path.$docs->name;?>&h=150&w=150" class="img-responsive">    
                <?php } elseif($docs->extension == 'docx' ) { ?>  
                  <img src="<?php echo base_url('');?>assets/timthumb.php?src=uploads/default/docx.png&h=150&w=150">            
                <?php } elseif($docs->extension == 'xsl') { ?>
                  <img src="<?php echo base_url('');?>assets/timthumb.php?src=uploads/default/xsl.png&h=150&w=150"> 
                <?php } elseif($docs->extension == 'pdf'){ ?>
                  <img src="<?php echo base_url('');?>assets/timthumb.php?src=uploads/default/pdf.jpg&h=150&w=150">
                <?php } elseif($docs->extension == 'txt'){ ?>
                  <img src="<?php echo base_url('');?>assets/timthumb.php?src=uploads/default/txt.png&h=150&w=150"> 
                <?php } else { ?>
                <img src="<?php echo base_url('');?>assets/timthumb.php?src=uploads/default/blank.png&h=150&w=150">
                <?php } ?>
                   <ul class="text-center">
                 <a href="<?php echo base_url('');?>user/project/deletedocs/<?php echo $docs->project_id.'/'.$docs->id ;?>"><li><i class="fa fa-trash-o" aria-hidden="true"></i></li></a>
                </ul>
              </div>
              
                <h1><?php echo $docs->name;?><h1>                 
                <h2><a href="<?php echo base_url('');?><?php echo $docs->path.$docs->name;?>" target="_blank">Download</a></h2>
            </div>
            <?php } ?>
          
           
         
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

