<div class="navbar navbar-inverse nav">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="/"><img src="<?php echo base_url('assets/admin/img/logo_white.png'); ?>"></a>
    		<div class="head-menu"><ul class="menu">
                            <li><a href="<?php base_url('');?>project"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
                            
                        </ul></div>
          	<div class="nav-collapses collapses">
              <div class="pull-right">
                <ul class="nav pull-right">
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome, User <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="/account/settings"><i class="icon-cog"></i> Settings</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo base_url('login/logout'); ?>"><i class="icon-off"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
              </div>
            </div>
        </div>
    </div>
</div>