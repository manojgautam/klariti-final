	<div class="container">
			<div class="row main">
				<div class="panel-heading">
	               <div class="panel-title text-center">
	               		<h1 class="title">Account Setting</h1>
	               		<hr />	               		       

	               	</div>
	            </div> 
				<div class="main-login main-center">
				 <?php if($this->session->flashdata('success')==true): ?>
                <div class="alert alert-success" role="alert">
                      <button class="close" data-dismiss="alert"></button>
                      <strong>Success: </strong><?php echo $this->session->flashdata('success'); ?>
                    </div>
        <?php endif;?>
					<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url('user/setting/updateprofile');?>">
				 <?php
        if (validation_errors()) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>
                <b><?php echo 'Alert'; ?>!</b><?php echo validation_errors(); ?>
            </div>

        <?php } ?>		
         <div class="profile-userpic">
                    
			<img class="img-responsive profileimg" src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo $product_image = ($userdetail[0]->profile_pic == 'default.png')?DEFAULT_IMAGE_URL . $userdetail[0]->profile_pic : BASE_IMAGE_URL . $userdetail[0]->profile_pic ; ?>&h=150&w=150">
<div class="imageprview"></div>
			<input type="file" name="userfile" id="img-prev" class="btn btn-primary fileupload">
                </div>
        
						<div class="form-group">
							<label for="f_name" class="cols-sm-2 control-label">First Name</label>
							<div class="cols-sm-10">									
									<input type="text" name="f_name" id="display_name" class="form-control input-lg" placeholder="First Name" value="<?php echo $userdetail[0]->f_name;?>" tabindex="3">
							</div>
						</div>

						<div class="form-group">
							<label for="l_name" class="cols-sm-2 control-label">Last Name</label>
							<div class="cols-sm-10">
									 <input type="text" name="l_name" id="display_name" class="form-control input-lg" placeholder="Last Name"  value="<?php echo $userdetail[0]->l_name;?>" tabindex="3" >
								
							</div>
						</div>

						<div class="form-group">
							<label for="city" class="cols-sm-2 control-label">City</label>
							<div class="cols-sm-10">
									 <input type="text" name="city" id="display_name" class="form-control input-lg" placeholder="city" tabindex="3" value="<?php echo $userdetail[0]->city;?>" >
								
							</div>
						</div>

						<div class="form-group">
							<label for="postcode" class="cols-sm-2 control-label">Postcode</label>
							<div class="cols-sm-10">
									 <input type="number" name="postcode" id="display_name" class="form-control input-lg" placeholder="Post Code" value="<?php echo $userdetail[0]->postcode;?>" tabindex="3" >
								
							</div>
						</div>

					<div class="form-group">
							<label for="mobile" class="cols-sm-2 control-label">Phone No</label>
							<div class="cols-sm-10">
									<input type="number" name="mobile" id="display_name" class="form-control input-lg"  placeholder="Mobile" tabindex="3" value="<?php echo $userdetail[0]->mobile;?>" required>
								
							</div>
						</div>
						<div class="form-group">
							<label for="address" class="cols-sm-2 control-label">Address</label>
							<div class="cols-sm-10">
									<textarea  class="form-control input-lg" name="address" placeholder="Address"><?php echo $userdetail[0]->address;?></textarea>
								
							</div>
						</div>

						<div class="form-group ">
							<input type="submit" name="update" value="Save Setting" class="btn btn-primary btn-block btn-lg" tabindex="7">
						</div>
						
					</form>
				</div>
			</div>
		</div>
