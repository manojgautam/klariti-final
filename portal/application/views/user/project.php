<div class="container">

<div class="row profile">

            <div class="profile-sidebar">
                <!-- SIDEBAR USERPIC -->

        <div class="col-md-6">
                <div class="profile-userpic">
                    
      <img class="img-responsive" src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo $product_image = ($userdetail[0]->profile_pic == 'default.png')?DEFAULT_IMAGE_URL . $userdetail[0]->profile_pic : BASE_IMAGE_URL . $userdetail[0]->profile_pic ; ?>&h=150&w=150">
     
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        <?php echo $userdetail[0]->username;?> 
                    </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR BUTTONS -->
              
                </div>
                <!-- END SIDEBAR BUTTONS -->
                <!-- SIDEBAR MENU -->
                 <div class="col-md-6">
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="active">
                            <a href="<?php echo base_url('user/project'); ?>" class="project">
                            <i class="glyphicon glyphicon-home"></i>
                            Projects </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('user/setting'); ?>" class="setting">
                            <i class="glyphicon glyphicon-user"></i>
                            Account Settings </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                            <i class="glyphicon glyphicon-ok"></i>
                            Report </a>
                        </li>
                        <li>
                            <a href="#">
                            <i class="glyphicon glyphicon-flag"></i>
                            Help </a>
                        </li>
                    </ul>
                </div>
                </div>
                <!-- END MENU -->
            </div>
        
</div>

<div class="container-fluid">
<a href="#" class="btn btn-primary addbtn">Add New</a>

<div class="row">
 <div class="col-md-6 col-sm-6">
    <div class="project_add" id="add-project" style="display:none;">
                        <h3>Please Fill Your Project Details</h3>
                     
	        
	 <form action="<?php echo base_url('user/project/project');?>" enctype="multipart/form-data" method="post">
	      	 <div class="col-md-6 col-sm-6">
	        <div class="form-group ">	       
	         <input type="text" name="title" id="title" class="form-control input-lg" placeholder="Project Title" tabindex="3"  required="required">
	        </div></div>
	         <div class="col-md-6 col-sm-6">
	         <div class="form-group">
                 <select id="curso" name="category"  class="form-control input-lg category"  tabindex="2" required>
                 <option value="">Please Select Category </option>
                    <?php foreach($categorlist as $cat){ ?>
                    <option value="<?php echo $cat->id; ?>"> <?php echo $cat->name; ?> </option>
                    <?php } ?>
                 </select>
                 </div>
		</div>
		  <div class="col-md-12 col-sm-12 ">
	         <div class="form-group subcategory">
                 <select id="curso" name="subcategory"  class="form-control input-lg subcat"  tabindex="2" required>                   
                    <option value="">Please Select Subcategory </option>                    
                 </select>
                 </div>
		</div>
		 <div class="col-md-12 col-sm-12">
	        <div class="form-group">
	         <textarea class="form-control input-lg" id="description" name="description" placeholder="Project Description"></textarea>                          
	        </div></div>
	       
	         <input type="submit" name="submit"  class="form-control btn btn-primary" >
	        
	 
	  </form>
	 
                        
                    </div></div>
</div>


	<div class="row">

     <?php foreach ($projectdetail as $project){ ?>
<div class="col-md-3 col-sm-4">
      <div class="wrimagecard wrimagecard-topimage">
          <a href="<?php echo base_url();?>user/project/projectdetail/<?php echo $project->id;?>">
          <div class="wrimagecard-topimage_header" style="background-color: rgba(119, 178, 88, 0.1)">
            <center><i class = "fa fa-cubes" style="color:#16A085"></i></center>
          </div>
          <div class="wrimagecard-topimage_title">
            <h4><?php echo $project->title; ?>
            <div class="pull-right badge" id="WrControls"></div></h4>
          </div>
        </a>
      </div>
</div>
	
 <?php }?>

</div>
</div>
</div>