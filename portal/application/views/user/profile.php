<div class="container">
    <div class="row profile">
        <div class="col-md-3">
            <div class="profile-sidebar">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    
			<img class="img-responsive" src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo $product_image = ($userdetail[0]->profile_pic == 'default.png')?DEFAULT_IMAGE_URL . $userdetail[0]->profile_pic : BASE_IMAGE_URL . $userdetail[0]->profile_pic ; ?>&h=250&w=250">
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        <?php echo $userdetail[0]->username;?> <span class="fa fa-envelope small pull-right"> </span>
                    </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR BUTTONS -->
                <div class="profile-userbuttons">                   
			<a href="<?php echo base_url('user/login/logout'); ?>" class="btn btn-lg btn-primary btn-block">Logout</a>
                </div>
                <!-- END SIDEBAR BUTTONS -->
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="active">
                            <a href="#project" class="project">
                            <i class="glyphicon glyphicon-home"></i>
                            Projects </a>
                        </li>
                        <li>
                            <a href="#setting" class="setting">
                            <i class="glyphicon glyphicon-user"></i>
                            Account Settings </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                            <i class="glyphicon glyphicon-ok"></i>
                            Tasks </a>
                        </li>
                        <li>
                            <a href="#">
                            <i class="glyphicon glyphicon-flag"></i>
                            Help </a>
                        </li>
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
        </div>
        <div class="col-md-9">
            <div class="profile-content" id="project">
    		<div class="row">
                <div class=" col-md-9">
                    <span class="button-checkbox">
                      <h3>  Projects Details</h3>
                                          
		<section>
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-folder-open"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </span>
                        </a>
                    </li>
                  </ul>
            </div>

    
                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="step1">
                        <h3>Please Fill Your Project Details</h3>
                     
	        
	 <form action="<?php echo base_url('user/project/project');?>" enctype="multipart/form-data" method="post">
	      	
	        <div class="form-group ">
	         <input type="text" name="title" id="title" class="form-control input-lg" placeholder="Project Title" tabindex="3"  required="required">
	        </div>
	        <div class="form-group">
	         <textarea class="form-control input-lg" id="description" name="description" placeholder="Project Description"></textarea>                          
	        </div>
	        
	         <ul class="list-inline pull-right">
                            <li><button type="button" id="projectid" class="btn btn-primary next-step">Save and continue</button></li>
                        </ul>
	  </form>
	 
                        
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step2">
                    <p class="msg alert alert-success" style="display:none;"> </p>
                        <h3>Add Your Documents</h3>	  
                         <form action="<?php echo base_url('user/project/fileupload');?>" enctype="multipart/form-data" method="post" class="dropzone">
			       <div class="fallback">
			        <input name="file[]" type="file" multiple />
			       </div>
			    </form>
			    
                        <ul class="list-inline pull-right">                           
                            <li><a href="<?php echo base_url('user/profile');?>" class="btn btn-primary "> Add Later </a></li>
                        </ul>
                    </div>
           
                    <div class="clearfix"></div>
                </div>
     
        </div>
    </section>
 
                        
                     
                    
                    <div class="panel-body">
<table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch"> 
	<thead>
		<tr>
			<th>Title</th>
			<th>Description</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
	  <?php foreach ($projectdetail as $project){ ?>
    <tr>
    <td><?php echo $project->title; ?></td>
    <td><?php echo $project->description; ?></td>
    <td><a href="#" data-toggle="modal" data-target="#documents_<?php echo $project->id; ?>">Add Document </a></td>
   </tr>
   
   
   
   
   	<!-- Modal for project documents -->
	<div class="modal fade" id="documents_<?php echo $project->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog modal-lg">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                <h4 class="modal-title" id="myModalLabel">Add Project Document</h4>
	            </div>
	     <div class="modal-body">
        	 
	
	   <form action="<?php echo base_url('user/project/fileupload');?>" enctype="multipart/form-data" method="post" class="dropzone">
	       <div class="fallback">
	        <input name="file[]" type="file" multiple />
	       </div>
	    </form>
		
	            </div>
	       
	        </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	</div><!-- /.modal for project documents -->
	
 <?php }?>

   </tbody>
</table>
</div>
                
                  
                   
                </div>
           
            </div>

             
        </div>
        
        <div class="setting-content" id="setting" style="display:none;">
        <h3> Account Setting</h3>
        <?php if($this->session->flashdata('success')==true): ?>
                <div class="alert alert-success" role="alert">
                      <button class="close" data-dismiss="alert"></button>
                      <strong>Success: </strong><?php echo $this->session->flashdata('success'); ?>
                    </div>
        <?php endif;?>
        <form action="<?php echo base_url('user/profile/setting');?>" enctype="multipart/form-data" method="post">
        <?php
        if (validation_errors()) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>
                <b><?php echo 'Alert'; ?>!</b><?php echo validation_errors(); ?>
            </div>

        <?php } ?>
         <div class="row">
         <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
         <input type="text" name="f_name" id="display_name" class="form-control input-lg" placeholder="First Name" value="<?php echo $userdetail[0]->f_name;?>" tabindex="3">
        </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
         <div class="form-group">
         <input type="text" name="l_name" id="display_name" class="form-control input-lg" placeholder="Last Name"  value="<?php echo $userdetail[0]->l_name;?>" tabindex="3" >
        </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
         <input type="number" name="mobile" id="display_name" class="form-control input-lg"  placeholder="Mobile" tabindex="3" value="<?php echo $userdetail[0]->mobile;?>" required>
        </div>
        </div>
     
        <div class="col-xs-12 col-sm-6 col-md-6">
           <div class="form-group">
         <input type="text" name="city" id="display_name" class="form-control input-lg" placeholder="city" tabindex="3" value="<?php echo $userdetail[0]->city;?>" >
        </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
         <input type="number" name="postcode" id="display_name" class="form-control input-lg" placeholder="Post Code" value="<?php echo $userdetail[0]->postcode;?>" tabindex="3" >
        </div>
        </div>
        
           <div class="col-xs-12 col-sm-12 col-md-12">
                 <div class="form-group">
        <textarea  class="form-control input-lg" name="address" placeholder="Address"><?php echo $userdetail[0]->address;?></textarea>
        </div>
        </div>
        <div class="col-xs-12 col-md-6"><input type="submit" name="update" value="Save Setting" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>   
         </div>
         </form>
        
        </div>
    </div>
</div>

	