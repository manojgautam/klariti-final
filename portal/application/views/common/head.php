<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title><?php if(isset($title)) echo $title; else echo "Klariti";?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="icon" type="image/x-icon" href="<?php echo base_url('assets/ico/favicon.png') ?> "/>
    <meta content="" name="description" />
    <meta content="" name="author" />

    <link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url('assets/bootstrap/bootstrap.min.css')?>" rel="stylesheet" type="text/css" media="screen" />
 <link href="<?php echo base_url('assets/dropzone/dropzone.css')?>" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
  </head>
