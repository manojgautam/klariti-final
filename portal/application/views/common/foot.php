<script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/custom.js')?>"></script>
<script src="<?php echo base_url('assets/dropzone/dropzone.js')?>"></script>

<script>



$(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {
    
    var title = $('#title').val();
    var description = $('#description').val();
    if(title.trim() != "" && description.trim() != ""){
    $.ajax({
               url:'<?php echo base_url();?>user/project/addprojectajax',
               type:'POST',
                data:{title: title,description: description},
               success:function(data){
                 console.log(data);
                 $('.msg').text("Project Added Successfully");
                 $('.msg').show();
               }
               
           });
           
           } else {
           if(title.trim() == ""){
           $('#title').css("border","1px solid red");
           }
           else{
            $('#title').css("border","1px solid #ccc");
           }
           if(description.trim() == ""){
           $('#description').css("border","1px solid red");
           }else{
           $('#description').css("border","1px solid #ccc");
           }
           
           return false;
           
           }

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
    
      $(".setting").click(function (){
      $("#setting").show();
       $("#project").hide();
      });
         $(".project").click(function (){
      $("#setting").hide();
       $("#project").show();
      });

    $(".addbtn").click(function (){
      $(".project_add").toggle();
      });

});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}
</script>
<!-- image preview -->
 <script type="text/javascript">
$(function() {
    // Single images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    //$($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    $(placeToInsertImagePreview).html($($.parseHTML('<img>')).attr('src', event.target.result))
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#img-prev').on('change', function() {
        imagesPreview(this, 'div.imageprview');
    });
 
});

$('body').on('change','.fileupload', function() {
  $('.profileimg').hide();
});

   </script>

<!--select category and subcategory-->
<script>
$(".category").change(function(){
var id =  $(this).val();
if(id != ''){
  $.ajax({
               url:'<?php echo base_url();?>user/project/getsubcatbyajax',
               type:'POST',
                data:{id: id},
               success:function(data){
                 console.log(data);
                 $('.subcategory').html(data);
                 $('.msg').text("Category Added Successfully");
                 $('.msg').show();
               }
               
           });
           }else{
           
            $('.subcategory').html('<select id="curso" name="subcategory"  class="form-control input-lg subcat"  tabindex="2" required><option value="">Please Select Subcategory </option></select>');
           }
 
});

</script>
<!--end category and subcategory -->


<script>
$(document).ready(function () {
var id= <?php echo $projectdetail[0]->id; ?>;
$('.addfile').prepend('<input type="hidden" value="'+id+'" name="projectid">');

});
</script>