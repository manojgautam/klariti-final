        <div class="login-wrapper ">
      <!-- START Login Background Pic Wrapper-->
      <div class="bg-pic">
        <!-- START Background Pic-->
        <img src="<?php echo base_url('assets/img/demo/login.jpg')?>" data-src="<?php echo base_url('assets/img/demo/login.jpg')?>" data-src-retina="<?php echo base_url('assets/img/demo/login.jpg')?>" alt="" class="lazy">
        <!-- END Background Pic-->
        <!-- START Background Caption-->
        <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
		   <img src="<?php echo base_url('assets/img/favens_logo_blue.png')?>" alt="logo" data-src="<?php echo base_url('assets/img/favens_logo_white.png')?>" data-src-retina="<?php echo base_url('assets/img/logo_2x.png')?>" width="150" >
          <p class="small">
           copyright &copy; 2017 Klariti.
          </p>
        </div>
        <!-- END Background Caption-->
      </div>
      <!-- END Login Background Pic Wrapper-->
      <!-- START Login Right Container-->
      <div class="login-container bg-white">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-100 sm-p-l-15 sm-p-r-15 sm-p-t-40">
		<img src="<?php echo base_url('assets/img/favens_logo_blue.png')?>" alt="logo" data-src="<?php echo base_url('assets/img/favens_logo_blue.png')?>" data-src-retina="<?php echo base_url('assets/img/logo_2x.png')?>" width="150" >
        
          
                     <?php if($this->session->flashdata('error')==true): ?>
                       <h5 style="color:red;"><?php echo $this->session->flashdata('error') ;?></h5>
                      <?php endif;?>
          
          <!-- START Login Form -->
          <form id="form-personal" role="form"  autocomplete="off" method="post" action="<?php echo base_url();?>admin/login/forgetpassword/<?php echo $id;?>">
                       <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>New Password</label>
                            <input type="password" class="form-control" name="password" id="" placeholder="New Password" required>
                            <?php echo form_error('password');?>
                          </div> 
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Confirm Password</label>
                            <input type="password" class="form-control" name="confirm_password" id="" placeholder="Confirm Password" required>
                            <?php echo form_error('confirm_password');?>
                          </div> 
                        </div>
                      </div>
                
       
                      <div class="clearfix"></div>
                     
                      <input type="submit" name="submitforgot" class="btn btn-primary" id="forget_submitpage" value="Change Password" >
                    </form>
          <!--END Login Form-->

        </div>
      </div>
      <!-- END Login Right Container-->
    </div>

           
