<div class="container">
 <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg bg-white">
            <!-- START PANEL -->
<div class="col-sm-4"></div>
<div class="col-sm-8">
            <div class="panel panel-transparent">
              <div class="panel-heading">
                <div class="panel-title">Projects
                </div>
                <div class="pull-right">
                  <div class="col-xs-12">
                    <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="panel-body">
                <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
                  <thead>
                    <tr>
                      <th>Title</th>
                      <th>Activities</th>
                      <th>Status</th>
                      <th>Last Update</th>
                    </tr>
                  </thead>
                  <tbody>
<?php foreach ($projectdetail as $project){ ?>
                    <tr>
                      <td class="v-align-middle semi-bold">
                        <p><?php echo $project->title; ?></p>
                      </td>
                    
                      <td class="v-align-middle">
                        <p><?php echo $project->description; ?></p>
                      </td>
                      <td class="v-align-middle">
                        <p>Public</p>
                      </td>
                      <td class="v-align-middle">
                        <p>April 13,2014 10:13</p>
                      </td>
                    </tr>
                    <tr>
         <?php } ?>
                  </tbody>
                </table>
              </div>
            </div></div>
            <!-- END PANEL -->
          </div>
          <!-- END CONTAINER FLUID -->
</div>