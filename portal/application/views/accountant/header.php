<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Pages - Admin Dashboard UI Kit - Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="<?php echo base_url('assets/admin/plugins/pace/pace-theme-flash.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/admin/plugins/bootstrapv3/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/admin/plugins/font-awesome/css/font-awesome.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/admin/plugins/jquery-scrollbar/jquery.scrollbar.css')?>" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url('assets/admin/plugins/select2/css/select2.min.css')?>" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url('assets/admin/plugins/switchery/css/switchery.min.css')?>" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url('assets/admin/plugins/nvd3/nv.d3.min.css')?>" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url('assets/admin/plugins/mapplic/css/mapplic.css')?>" rel="stylesheet" type="text/css" />
   //<link href="<?php //echo base_url('assets/admin/plugins/rickshaw/rickshaw.min.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/admin/plugins/bootstrap-datepicker/css/datepicker3.css')?>" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url('assets/admin/plugins/jquery-metrojs/MetroJs.css')?>" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url('assets/admin/css/pages-icons.css')?>" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="<?php echo base_url('assets/admin/css/pages.css')?>" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url('assets/admin/css/style.css')?>" rel="stylesheet" type="text/css" media="screen" />
    <!--[if lte IE 9]>
	<link href="<?php echo base_url('assets/admin/plugins/codrops-dialogFx/dialog.ie.css')?>" rel="stylesheet" type="text/css" media="screen" />
	<![endif]-->
  </head>