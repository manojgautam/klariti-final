<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Login_model', 'loginmodel');
      
	}
		
	function index() {
				
	       $submitted = $this->input->post('submitted');
			if ($submitted)
			{
	
				$email	= $this->input->post('email');
				$password	= $this->input->post('password');
				$login		= $this->loginmodel->check_login_details($email, $password);
			
			
				if (count($login))
				{
				    //admin session start
				    $userdata= array( 'email'=> $email, 
				    'id' => $login[0]->id,
	                         'email_verified' => $login[0]->email_verified
				   );
				        
				        
				$this->session->set_userdata('usersession', $userdata);
				if($login[0]->user_type == 'standard'){	 redirect(base_url().'user/project');   } 
				elseif($login[0]->user_type == 'accountant') {  redirect(base_url().'accountant/home');  } 
				else{redirect(base_url().'admin/dashboard'); }                   
	
				}
				else
				{
					//this adds the redirect back to flash data if they provide an incorrect credentials
			                $checkdeactivate= $this->loginmodel->check_deactivate_details($email, $password);
	                               if(count($checkdeactivate)){$this->session->set_flashdata('error', 'User has been deactivated');}
	                               else{
					$this->session->set_flashdata('error', 'Email/Password is not correct');}
					redirect(base_url().'login');
				}
			}
			
			       $data['title'] = "Klariti | Admin Login";
			
				$this->load->view('common/head',$data);	
				$this->load->view('login');	
				$this->load->view('common/foot');
	}	

      function forgot(){
		        $email=$this->input->post('email');
		        $data=$this->loginmodel->Checkemailforgot($email);
		
		        if(count($data))
		        {$id=base64_encode($data[0]->id);
		        $data_email['name']=$data[0]->name;
		        $data_email['message']="<p>You  recently requested to reset your password for your Favens Account. Click the button below to reset it</p> ";
		        $data_email['link']=base_url().'admin/login/forgetpassword/'.$id;
		        $data_email['message1']="<p> if you dont request a password reset,Please ignore this email or reply to let us know.</p>";
		        $data_email['link_text']="Click here to reset your password";
		        $message=$this->load->view('admin/email',$data_email,true);
		        $this->globalmailfunction($email,"Klariti - Reset Password",$message);
		         }
		        else{
		        echo "not match";
		            }
      }

    function forgetpassword(){
			$id=base64_decode($this->uri->segment(4));		
			$data['title'] = "Klariti | Admin Login";
			$data['id'] = $this->uri->segment(4);
			
			  if($this->input->post('submitforgot')==true)
			         {
			            $this->form_validation->set_rules('password','Password','required');
			            $this->form_validation->set_rules('confirm_password','Confirm Password','required|matches[password]');
			            $this->form_validation->set_message('required','The passwords are not the same');
			            $this->form_validation->set_error_delimiters('<span style="color: #f55753;font-size: 12px;">','</span>');
			            if($this->form_validation->run()==true)
			           {
			          $data=array(
			          'password'=>md5($this->input->post('password'))
			          );
			            $this->loginmodel->updateForgetpassword($id,$data);
			           redirect(base_url());
			         }
			              else{
			                        $this->load->view('common/head',$data);	
						$this->load->view('forgot');	
						$this->load->view('common/footer');
			                  }
			}
			else{
			                        $this->load->view('common/head',$data);	
						$this->load->view('forgot');	
						$this->load->view('common/foot');
			}
					
						
		
	}
	
	function logout(){ 
	
		    $this->session->unset_userdata('usersession');
		    redirect(base_url().'login');
	    
	}
}