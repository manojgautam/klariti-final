<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends MY_Controller {

	public function __construct()
	{
		  parent::__construct();
    		   $this->load->model('Register_model', 'registermodel');       
        }


	function index() {

            if($this->input->post('submit') == true){ 
       
		         $this->form_validation->set_rules('fname', 'Firstname', 'required');
		         $this->form_validation->set_rules('lname', 'Lastname', 'required');
		         $this->form_validation->set_rules('usertype', 'Lastname', 'required');
		         $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
		         $this->form_validation->set_rules('password', 'Password', 'trim|min_length[4]');
			 $this->form_validation->set_rules('password_confirmation', 'Confirm password', 'required|matches[password]');      
	      	         $this->form_validation->set_error_delimiters('<span style="color: red;font-size: 12px;display: inline-block;line-height: 18px;margin-top: 6px;">','</span>');
      
        	   if ($this->form_validation->run() == TRUE){
         		  //echo "<pre>";print_r($_POST);die;
		                $email = $this->input->post('email');
				$password = md5($this->input->post('password'));
		  	        $current_date_time = date("Y-m-d H:i:s");
		                $register_array = array (
		                      'f_name' => $this->input->post('fname'),
		                      'l_name'=>$this->input->post('lname'),
		                      'email'=>$this->input->post('email'),
		                      'password'=>$password,
		                      'username'=>$this->input->post('username'),
		                      'user_type'=>$this->input->post('usertype'),
		                      'created_at' => $current_date_time
		                   );
	
	               		 $last_id = $this->registermodel->add_user($register_array);
	              
			              $data_email['name']=$this->input->post('f_name').' '.$this->input->post('l_name');   
			              $data_email['message']="You have been successfully registered with Klariti. Click the button below to Activate your account ";           
			              $data_email['link']=base_url().'Register/userActive/'.base64_encode($last_id);    
			              $data_email['message1']="please visit the site";   
			              $data_email['link_text']="Activate Account";     
			              $message=$this->load->view('email',$data_email,true);  
			              $this->globalmailfunction($email,"Klariti user Activation ",$message);			
				      $this->session->set_flashdata('success',"Please Check You Mail And Verify Your Account");                   
		 	
                         }
 
                  }
		                $email =   $this->session->userdata('adminsession')['email'];
			    	$data['title'] = "Klariti | User Registeration";
			        $data['aaduser'] = "adduser";
		                     
		               $this->load->view('common/head');
		               $this->load->view('register');	
		               $this->load->view('common/foot');
       }


       function userActive($uid){
      
        	         $userid = base64_decode($uid); 
	     		    $data = array (
	                      'email_verified' => 1                      
	                   );
	
		     $stat = $this->registermodel->update_emailverified($userid, $data);
                     $this->session->set_flashdata('success',"Record Added Successfully");                 
		     redirect(base_url().'register');

          }


}