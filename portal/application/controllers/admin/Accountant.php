<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accountant extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
           $this->load->model('admin/Accountant_model', 'accountmodel');
                 
      
    }
        
    function index() {

                $data['allaccountant'] = $this->accountmodel->get_all_user();

       		$data['title'] = "Klariti | Accountant";
                $this->load->view('admin/header',$data);
                $this->load->view('admin/sidebar'); 
                $this->load->view('admin/accountant');  
                $this->load->view('admin/footer');   
        }
}
