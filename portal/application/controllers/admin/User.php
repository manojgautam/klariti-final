<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
           $this->load->model('admin/User_model', 'usermodel');

          
      
    }
        
    function index() {

      	        $data['alluser'] = $this->usermodel->get_all_user();

       		$data['title'] = "Klariti | Users";
                $this->load->view('admin/header',$data);
                $this->load->view('admin/sidebar'); 
                $this->load->view('admin/user');  
                $this->load->view('admin/footer');   
        }
}
