<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
          
                        $this->load->model('admin/Categories_model', 'categoriesmodel');
      	
        
      
    }
        
    function index() {
		    		    
			  $submit = $this->input->post('submit');
				if ($submit)
				{
				    $current_date_time = date("Y-m-d H:i:s");						 
				    $categories_array = array(
							'name' => $this->input->post('cname'),	
							'parent' => '0',								
							'created_at' => $current_date_time
					);
					
				$this->categoriesmodel->add_categories($categories_array);
				$this->session->set_flashdata('success',"Record Added Successfully");
				}
								
				$email =   $this->session->userdata('adminsession')['email'];
				$data['title'] = "Klariti | Admin Categories";
				//$data['categorlist'] = $this->categoriesmodel->get_all_categories();
				$data['categorlist'] = $this->categoriesmodel->get_categories();
				
				
				//echo "<pre>"; print_r($data['categorlist']); die;
                    
                
           


                $this->load->view('admin/header',$data);
                $this->load->view('admin/sidebar'); 
                $this->load->view('admin/categories');  
                $this->load->view('admin/footer');   
        }

        function addsubcategory() {
				$submit = $this->input->post('submit');
				if ($submit)
				{
				    $current_date_time = date("Y-m-d H:i:s");						 
				    $categories_array = array(
							'name' => $this->input->post('csubname'),
							'parent' => $this->input->post('category'),									
							'created_at' => $current_date_time
					);
					//print_r($categories_array);die;
					
				$this->categoriesmodel->add_categories($categories_array);
				$this->session->set_flashdata('success',"Record Added Successfully");
				}
								
				
                    $email =   $this->session->userdata('adminsession')['email'];
				$data['title'] = "Klariti | Admin Categories";
				$data['categorlist'] = $this->categoriesmodel->get_categories();
				
                
           


                $this->load->view('admin/header',$data);
                $this->load->view('admin/sidebar'); 
                $this->load->view('admin/categories');  
                $this->load->view('admin/footer');   

         }
          function updatecategory($id) {
          
            $submit = $this->input->post('submit');
				if ($submit)
				{
				  						 
				    $categories_array = array(
							'name' => $this->input->post('csubname'),	
							'parent' => '0'
					);
					
				$this->categoriesmodel->update_category($categories_array, $id);
				$this->session->set_flashdata('success',"Record Updated Successfully");
				}
				redirect(base_url().'admin/categories');
								
          }
          
           function updatesubcategory($id) {
          
            $submit = $this->input->post('submit');
				if ($submit)
				{
				  						 
				    $categories_array = array(
							'name' => $this->input->post('csubname')	
							
					);
					
				$this->categoriesmodel->update_category($categories_array, $id);
				$this->session->set_flashdata('success',"Record Updated Successfully");
				}
				redirect(base_url().'admin/categories');
								
          }


        function categoriesdelete($id){

		    $data = $this->categoriesmodel->delete_categories($id);
	       	redirect(base_url().'admin/categories');
		    
	}
	
	
	
	function subcategoriesdelete($id){

		    $data = $this->categoriesmodel->delete_subcategories($id);
	       	redirect(base_url().'admin/categories');
		    
	}
	
	
	function addcategoryajax(){
          		       
		        		
		            $current_date_time = date("Y-m-d H:i:s");
             		     $categories_array = array(
							'name' => $this->input->post('name'),	
							'parent' => '0',								
							'created_at' => $current_date_time
					);
              
              		    $this->categoriesmodel->add_categories($categories_array);
              		    $categories =  $this->categoriesmodel->get_all_categories();
              		   
              		    ?>
              		    <select name="category" class="full-width" id="categrysel" data-placeholder="Select Category" data-init-plugin="select2">    
	                    <?php if($categories): foreach($categories as $cat): ?>
	                    <option value="<?php echo $cat->id; ?>"> <?php echo $cat->name; ?> </option>
	                    <?php endforeach; else:?>
				<p>No Category Found</p>
				<?php endif; ?>
               		   </select>
              		    
              		    
              	<?php	    
	               	                   
               
          }
          
          function addsubcategoryajax(){
          		       
		        		
		            $current_date_time = date("Y-m-d H:i:s");
             		     $categories_array = array(
							'name' => $this->input->post('csubname'),
							'parent' => $this->input->post('category'),									
							'created_at' => $current_date_time
					);
              
              		    $this->categoriesmodel->add_categories($categories_array);              		    
	       
          }
          
  
	
	
}
