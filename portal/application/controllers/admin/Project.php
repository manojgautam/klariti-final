<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
           $this->load->model('admin/Project_model', 'projectmodel');
           $this->load->model('admin/User_model', 'usermodel');
                 
      
    }
        
    function index() {

                $data['allproject'] = $this->projectmodel->get_all_project();
                $data['alluser'] = $this->usermodel->get_all_user();

       		$data['title'] = "Klariti | Projects";
                $this->load->view('admin/header',$data);
                $this->load->view('admin/sidebar'); 
                $this->load->view('admin/project');  
                $this->load->view('admin/footer');   
        }
}
