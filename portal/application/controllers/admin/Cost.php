<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cost extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
          
                        $this->load->model('admin/Categories_model', 'categoriesmodel');
      	                $this->load->model('admin/Cost_model', 'costmodel');
      	
        
      
    }
        
    function index() {
		    	
	  $submit = $this->input->post('submit');
				if ($submit)
				{
				    $current_date_time = date("Y-m-d H:i:s");						 
				    $cost_array = array(
							'name' => $this->input->post('costname'),	
							'description' => $this->input->post('costdesc'),
                                                         'cost' => $this->input->post('cost'),	
							'category_id' => $this->input->post('category'),	
							'subcategory_id' => $this->input->post('subcategory'),
							
							'created_at' => $current_date_time
					);
					
				$this->costmodel->add_cost($cost_array);
				$this->session->set_flashdata('success',"Record Added Successfully");
				}	    
	
								
				$email =   $this->session->userdata('adminsession')['email'];
				$data['title'] = "Klariti | Admin Categories";
				//$data['categorlist'] = $this->categoriesmodel->get_all_categories();
				$data['categorlist'] = $this->categoriesmodel->get_categories();
				$data['costslab'] = $this->costmodel->get_all_cost();
				
				//echo "<pre>"; print_r($costslab); die;
                    
                
           


                $this->load->view('admin/header',$data);
                $this->load->view('admin/sidebar'); 
                $this->load->view('admin/addcost');  
                $this->load->view('admin/footer');   
        }
        function listcost() {

                             $email =   $this->session->userdata('adminsession')['email'];
				$data['title'] = "Klariti | Admin Categories";
				//$data['categorlist'] = $this->categoriesmodel->get_all_categories();
				$data['categorlist'] = $this->categoriesmodel->get_categories();
				$data['costslab'] = $this->costmodel->get_all_cost();
				
				//echo "<pre>"; print_r($costslab); die;
	

                $this->load->view('admin/header',$data);
                $this->load->view('admin/sidebar'); 
                $this->load->view('admin/cost');  
                $this->load->view('admin/footer');
        }

 

       function getsubcatbyajax(){
           $id =  $this->input->post('id');
            $subcategory = $this->categoriesmodel->get_subcategories_byid($id);
            	    ?>
            	    <label class="">Select Sub Category</label>
                 <select id="subcurso" name="subcategory" class="full-width input-lg subcat" data-init-plugin="select2" required>                   
                     
	                    <?php if($subcategory): foreach($subcategory as $cat): ?>
	                    <option value="<?php echo $cat->id; ?>"> <?php echo $cat->name; ?> </option>
	                    <?php endforeach; else:?>
				 <option value=""> No Subcategory found </option>
				<?php endif; ?>
               		   </select>
              		    
              		    
              	<?php  
	               	                    
         }
          
  
	
	
}
