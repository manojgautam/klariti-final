<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
          if (!$this->checksession()) {
            redirect(base_url() . 'login');
        }
        
      
    }
        
    function index() {
                $this->load->view('admin/header');
                $this->load->view('admin/sidebar'); 
                $this->load->view('admin/dashboard');  
                $this->load->view('admin/footer');   
        }
}
