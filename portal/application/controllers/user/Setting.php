<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
           $this->load->model('user/Profile_model', 'profilemodel');
       
        $this->load->helper('string');
      
    }
        
    function index() {

       		$email = $this->session->userdata('usersession')['email'];
      	        $data['userdetail'] = $this->profilemodel->get_user_details($email);

       		$data['title'] = "Klariti | Profile";
                $this->load->view('common/head',$data); 
                 $this->load->view('user/header');  
                $this->load->view('user/setting');  
                $this->load->view('common/foot');   
        }


       function updateprofile(){
          	 $update = $this->input->post('update');
      		 $id = $this->session->userdata('usersession')['id'];
      		 
       		 if ($update) {

       		 
	                 $this->form_validation->set_rules('postcode', 'Postcode', 'trim|max_length[6]');
	     		 $this->form_validation->set_rules('mobile', 'Mobile', 'trim|max_length[10]|min_length[10]');
     		 
        	 	 if ($this->form_validation->run() == true) {

                     /* image upload start */
			 $config['upload_path']='./uploads/profile/';
			 $config['allowed_types']='jpg|jpeg|png|JPG|JPEG|PNG';
			 $config['file_name']=random_string('alnum', 10).".jpg";
			 $this->load->library('upload');
			 $this->upload->initialize($config);
			 
			  if(!$this->upload->do_upload())
			   { 
			     $image='';			 
			    }
				else
				{ 
				$imgdata=$this->upload->data();
				$image=$imgdata['file_name'];  
				}
				/* image upload end */
        
	            	    $setting_array = array(
	                    'id' => $id,
	                    'f_name' => $this->input->post('f_name'),
	                    'l_name' => $this->input->post('l_name'),  
	                    'mobile' => $this->input->post('mobile'),
	                    'address' => $this->input->post('address'),  
	                     'city' => $this->input->post('city'),
			     'profile_pic' => $image,
	                    'postcode' => $this->input->post('postcode')
	             	   );
                             
	                 $this->profilemodel->update_user_setting($setting_array, $id);
	                 $this->session->set_flashdata('success', "Profile updated Successfully");
	                 redirect(base_url() . 'user/setting');
	        
      		   } else{
          	    $data['userdetail'] = $this->profilemodel->get_user_details($this->session->userdata('usersession')['email']);

       		    $data['title'] = "Klariti | Profile";
                    $this->load->view('common/head',$data);   
	            $this->load->view('user/setting');  
	            $this->load->view('common/foot');  
          	    }
       		 }
          }

}