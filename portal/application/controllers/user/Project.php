<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

              $this->load->model('user/Profile_model', 'profilemodel');
       		 $this->load->model('user/Project_model', 'projectmodel');
		$this->load->model('admin/Categories_model', 'categoriesmodel');
      	
        	
      
    }

    function index() {

       		$email = $this->session->userdata('usersession')['email'];
      	        $data['userdetail'] = $this->profilemodel->get_user_details($email);

                $id = $this->session->userdata('usersession')['id'];
      	        $data['projectdetail'] = $this->projectmodel->get_project_detail($id);
      	        
                  $data['categorlist'] = $this->categoriesmodel->get_all_categories();
				

                $data['title'] = "Klariti | Project";
                $this->load->view('common/head',$data);
                $this->load->view('user/header');  
                $this->load->view('user/project');  
                $this->load->view('common/foot'); 
     }


     function fileupload(){
		  $target_dir = "uploads/profile/";
                        $target_file = $target_dir . basename($_FILES["file"]["name"]);
                        $newfilename=basename($_FILES["file"]["name"]);
                    $extenstion = explode('.',$_FILES["file"]["name"]);
                      $filename = array();
                      
                for( $i=0; $i<count($extenstion)-1; $i++){
                         $filename[]= $extenstion[$i];
                      }
                  $fstname= implode('.',$filename);
                  
                  if(file_exists($target_file)){
                    $newfilename=$fstname.'_'.time().'.'.$extenstion[count($extenstion)-1];
        $target_file = $target_dir .$newfilename;
        //print_r($target_file);
        
      }
      $type = $extenstion[count($extenstion)-1];
      
      $this->session->set_userdata('filesession',$newfilename);
      
                  if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
                               echo $newfilename;
                               echo $this->input->post('projectid');
                               $data= array(
                                'project_id'=> $this->input->post('projectid'),
                               'name' => $newfilename,
                               'path' =>$target_dir,
                               'extension'=> $extenstion[count($extenstion)-1]
                               
                               );
                              $this->projectmodel->add_project_docs($data);
                          } else {
                              echo "Sorry, there was an error uploading your file.";
                        }


         }
         

      function project(){
          
		        $submit = $this->input->post('submit');
		        
		        if ($submit) {
		        		
		            $current_date_time = date("Y-m-d H:i:s");
             		    $project_array = array(
                   	   'user_id' => $this->session->userdata('usersession')['id'],
                  	   'title' => $this->input->post('title'),
                 	   'description' => $this->input->post('description'), 
                 	   'category' =>$this->input->post('category'),
                 	   'subcategory' =>$this->input->post('subcategory'),              
                 	   'created_at' => $current_date_time
              		  );
              
              		    $project = $this->projectmodel->add_project($project_array);
	                    $this->session->set_flashdata('success', "Project Added Successfully");
	                    redirect(base_url() . 'user/project');
	                    }
               
          }

       function projectdetail($id){
	                 //$id = $this->session->userdata('usersession')['id'];
	                $data['projectdetail'] = $this->projectmodel->get_single_project($id);
	                 $data['docsdetail'] = $this->projectmodel->get_single_project_docs($id);
	                
	                $data['title'] = "Klariti | Single Project";
	                $this->load->view('common/head',$data);
	                $this->load->view('user/header');  
	                $this->load->view('user/projectdetails');  
	                $this->load->view('common/foot'); 

      }
          
          
      function addprojectajax(){
          		       
		        		
		            $current_date_time = date("Y-m-d H:i:s");
             		    $project_array = array(
                   	   'user_id' => $this->session->userdata('usersession')['id'],
                  	   'title' => $this->input->post('title'),
                 	   'description' => $this->input->post('description'),              
                 	   'created_at' => $current_date_time
              		  );
              
              		    $project = $this->projectmodel->add_project($project_array);
              		    echo $project;
	               	                   
               
          }
         
         function getsubcatbyajax(){
           $id =  $this->input->post('id');
            $subcategory = $this->categoriesmodel->get_subcategories_byid($id);
            	    ?>
              		    <select name="subcategory" class="form-control input-lg subcat" id="categrysel" data-placeholder="Select Category" data-init-plugin="select2">    
	                    <?php if($subcategory): foreach($subcategory as $cat): ?>
	                    <option value="<?php echo $cat->id; ?>"> <?php echo $cat->name; ?> </option>
	                    <?php endforeach; else:?>
				 <option value=""> No Subcategory found </option>
				<?php endif; ?>
               		   </select>
              		    
              		    
              	<?php  
	               	                    
         }
         
          function deletedocs($pid,$id){
          $this->projectmodel->delete_project_docs($id);
          redirect(base_url() . 'user/project/projectdetail/'.$pid);
	                
          }
         


}
       