<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
           $this->load->model('user/Profile_model', 'profilemodel');
           $this->load->model('user/Project_model', 'projectmodel');
       	    if (!$this->checksession()) {
            redirect(base_url() . 'login');
        }
      
    }
        
    function index() {

       		$email = $this->session->userdata('usersession')['email'];
       		$id = $this->session->userdata('usersession')['id'];
      	        $data['userdetail'] = $this->profilemodel->get_user_details($email);
      	        $data['projectdetail'] = $this->projectmodel->get_project_detail($id);

       		$data['title'] = "Klariti | Profile";
                $this->load->view('common/head',$data); 
                 $this->load->view('user/header');  
                $this->load->view('user/profile');  
                $this->load->view('common/foot');   
        }


          
     



}