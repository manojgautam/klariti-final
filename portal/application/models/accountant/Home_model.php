<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model 
{
	function __construct()
	{
		parent::__construct();
	
	}

	
	public function get_project_detail($id){
	
	        $this->db->select('*');
                $this->db->from('projects');                
	         $this->db->where('user_id', $id);
        	 $Q = $this->db->get();
     		 //print_r($Q->result());
      		 return $Q->result();
		
	}
	

	public function get_single_project($id) {
	        $this->db->select('*');
	        $this->db->from('projects');  
	        $this->db->where("id",$id);
	        $q=$this->db->get();
	        return $q->result();
       }

}
	