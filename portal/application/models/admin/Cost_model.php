<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cost_model extends CI_Model 
{
	function __construct()
	{
		parent::__construct();
	
	}
	
	public function add_cost($data){
  
	            $return = $this->db->insert('cost_slabs', $data);
	            if ((bool) $return === TRUE) {
	                return $this->db->insert_id();
	            } else {
	                return $return;
	            }       
				
		}
		
	public function update_cost($data,$id){
		 $this->db->where("id",$id);
		 $this->db->update('cost_slabs',$data);
	}


	
	public function get_all_cost() {
     
	        $this->db->select('cost_slabs.id, cost_slabs.name as slabname, cost_slabs.description, cost_slabs.cost, cost_slabs.category_id, cost_slabs.subcategory_id, cat.name as catname,subcat.name as subcatname');
	        $this->db->from('cost_slabs');
                $this->db->join('categories as cat','cost_slabs.category_id = cat.id','left');  
                $this->db->join('categories as subcat','cost_slabs.subcategory_id = subcat.id','left');      
 	        $q=$this->db->get();
	        $all= $q->result();
	      
	      //foreach($all as $val){
	     // $val->subcategory_id = $this->getsubcategory($val->subcategory_id);
	     // $val->category_id = $val->name;
	     // }	       
	     // echo "<pre>"; print_r($all); die; 
	        return $all;
        }
        
    
        
        
       
	
	
		
}
    