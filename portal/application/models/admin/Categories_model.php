<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories_model extends CI_Model 
{
	function __construct()
	{
		parent::__construct();
	
	}
	
	public function add_categories($data){
  
	            $return = $this->db->insert('categories', $data);
	            if ((bool) $return === TRUE) {
	                return $this->db->insert_id();
	            } else {
	                return $return;
	            }       
				
		}
		
	public function update_category($data,$id){
		 $this->db->where("id",$id);
		 $this->db->update('categories',$data);
	}


	
	public function get_all_categories() {
     
	        $this->db->select('*');
	        $this->db->order_by('id','desc');
	        $this->db->from('categories'); 
                $this->db->where('parent', 0);   
 	        $q=$this->db->get();
	        $all= $q->result();
	        foreach($all as $val){
               $val->project_count=$this->getprojectCount($val->id);
        }
	        return $all;
        }
        
        
        
	public function get_categories() {
     
	        $this->db->select('*');
	        $this->db->order_by('id','desc');
	        $this->db->from('categories'); 	        
                $this->db->where('parent', 0);   
 	        $q=$this->db->get();
	        $all= $q->result_array();
	        $i=0;
	        foreach($all as $val){
               $all[$i]['project_count']=$this->getprojectCount($val['id']);
               $all[$i]['subcategories']=$this->get_subcategories_byid_array($val['id']);
               $i++;
        }
	        return $all;
        }
        
        public function get_subcategories_byid_array($id) {
     
	        $this->db->select('*');
	        $this->db->order_by('id','desc');
	        $this->db->from('categories'); 
                $this->db->where('parent', $id);   
 	        $q=$this->db->get();
	        $all= $q->result_array();	       
	        return $all;
        }

	public function get_subcategories_byid($id) {
     
	        $this->db->select('*');
	        $this->db->order_by('id','desc');
	        $this->db->from('categories'); 
                $this->db->where('parent', $id);   
 	        $q=$this->db->get();
	        $all= $q->result();	       
	        return $all;
        }

       public function delete_categories($id){

		$this->db->where('id', $id);
		$this->db->delete('categories');
		
		$this->db->where('parent', $id);
		$this->db->delete('categories');
		

	}

      public function delete_subcategories($id){

		$this->db->where('id', $id);
		$this->db->delete('categories');
		
		

	}

      public function getprojectCount($id){
        $this->db->select('*');
        $this->db->from('projects');
        $this->db->where("category",$id);
	     $Q = $this->db->get();
         $res= $Q->result();

         $data=array('project_count'=>count($res));
        $this->db->where('id', $id);
        $this->db->update('categories', $data);
        
        return count($res);
        
    }
	
	
		
}
    