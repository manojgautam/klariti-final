<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project_model extends CI_Model 
{
	function __construct()
	{
		parent::__construct();
	
	}
	
	public function get_all_project() {
	         $this->db->select('projects.id, projects.user_id, projects.title, projects.description, projects.status, projects.bid_count,projects.category, projects.subcategory, cat.name as catname, subcat.name as subcatname, users.username');
	         $this->db->from('projects');	         
                  $this->db->join('users','projects.user_id = users.id','left');
                  $this->db->join('categories as cat','projects.category = cat.id','left');
                   $this->db->join('categories as subcat','projects.subcategory = subcat.id','left');
     		 //print_r($Q->result());
                 $Q = $this->db->get();
      		 $result = $Q->result();
      		 
      		//echo "<pre>"; print_r($result); die;
      		 return($result);
       }
	
	
		
}
    