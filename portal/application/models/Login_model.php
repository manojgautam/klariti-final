<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model 
{
	function __construct()
	{
		parent::__construct();
	
	}
	
	
	public function check_login_details($email, $password) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email', $email);
		$this->db->where('password', md5($password));
		$this->db->where('email_verified', 1);
		$this->db->limit(1);
        $Q = $this->db->get();
       
        return $Q->result();
    }


public function check_deactivate_details($email, $password) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email', $email);
		$this->db->where('password', md5($password));
		$this->db->where('email_verified', 0);
		$this->db->limit(1);
        $Q = $this->db->get();
       
        return $Q->result();
    }

public function Checkemailforgot($email){
     $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email', $email);
		$this->db->limit(1);
        $Q = $this->db->get();
       return $Q->result();
}

public function updateForgetpassword($id,$data){
  $this->db->where('id', $id);
  $this->db->update('users', $data);

}


}	