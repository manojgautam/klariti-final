<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project_model extends CI_Model 
{
	function __construct()
	{
		parent::__construct();
	
	}

	public function add_project($data){
  
            $return = $this->db->insert('projects', $data);
            if ((bool) $return === TRUE) {
                return $this->db->insert_id();
            } else {
                return $return;
            }       
			
	}
	
	public function add_project_docs($data){
  
            $return = $this->db->insert('project_docs', $data);
            if ((bool) $return === TRUE) {
                return $this->db->insert_id();
            } else {
                return $return;
            }       
			
	}
	public function get_project_detail($id){
	
	        $this->db->select('*');
                $this->db->from('projects');                
	         $this->db->where('user_id', $id);
        	 $Q = $this->db->get();
     		 //print_r($Q->result());
      		 return $Q->result();
		
	}
	

	public function get_single_project($id) {
	        $this->db->select('*');
	        $this->db->from('projects');  
	        $this->db->where("id",$id);
	        $q=$this->db->get();
	        return $q->result();
       }
       
	public function get_single_project_docs($id) {
	        $this->db->select('*');
	        $this->db->from('project_docs');  
	        $this->db->where("project_id",$id);
	        $q=$this->db->get();
	        return $q->result();
       }
       public function delete_project_docs($id) {
       $detail = $this->db->get_Where('project_docs', array('id'=> $id))->result();
       @unlink('uploads/profile/'.$detail[0]->name);
	        $this->db->where("id",$id);
		$this->db->delete('project_docs');
       }

}
	