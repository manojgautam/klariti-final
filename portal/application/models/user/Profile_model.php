<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile_model extends CI_Model 
{
	function __construct()
	{
		parent::__construct();
	
	}
	
	public function get_user_details($email) {
	         $this->db->select('*');
	         $this->db->from('users');
	         $this->db->where('email', $email);
	   	 $this->db->where('email_verified', 1);
		 $this->db->limit(1);
        	 $Q = $this->db->get();
     		// print_r($Q->result());
      		 return $Q->result();
       }

	public function update_user_setting($data,$id){
		 $this->db->where("id",$id);
		 $this->db->update('users',$data);
	}
	
		
}
    